# Testing Details

### Checkpoint 1

1. Test RTC and Keyboard
    a. Comment out `#define RUN_TESTS` in `kernel.c` to allow continuous interrupts.
    b. Select an output format in `rtc.c:68` to either print characters or an stream of messages.
    c. Compile and run.
    d. Repeat b and c with the other output format (if desired) and with both outputs commented out
        (disabling both outputs allows keyboard input to be seen)

2. Test IDT/Paging
    a. Uncomment `#define RUN_TESTS` in `kernel.c` to run the test suite and disable RTC/keyboard interrupts
    b. Select one or more tests from `test.c:206`
        NOTE: some tests are designed to halt while some will return normally.
    c. Repeat for all tests in `test.c`
