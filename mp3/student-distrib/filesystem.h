/* filesystem.h - Defines used in interactions with
 * the file system.
 */

#ifndef _FILESYSTEM_H
#define _FILESYSTEM_H

#include "types.h"

/* Variable sizes chosen to match appendix A spec.*/
#define BOOT_BLOCK_RESERVED     52              /* Reserved space in boot block (52 bytes)          */
#define DENTRY_RESERVED         24              /* Reserved space in dir. entries (24 bytes)        */

#define FILENAME_SIZE           32              /* Max length of filename                           */
#define BLOCK_SIZE              _4KB            /* Size of filesystem blocks                        */
#define DBLOCKS_PER_INODE       1023            /* Max number of data blocks referenced by an inode */
#define MAX_DENTRY              63              /* Max number of dir. entries in boot blocks        */

/**
 * Struct for dir. entry objects (size = 64 bytes)
 *     Dir entries are stored in the boot block and contain
 *     information about individual files, including the name,
 *     type, and which inode corresponds to the file.
 */
typedef struct dentry_struct {
    int8_t      filename[FILENAME_SIZE];        // 32-byte string, not necessarily with a null terminator
    uint32_t    filetype;                       // 4 B file type: 0 = RTC, 1 = Directory, 2 = file
    uint32_t    inode_number;                   // 4 B inode number
    uint8_t     reserved[DENTRY_RESERVED];      // Pads out to 64 bytes
 } dentry_t;

 /**
  * Struct for boot block (size = 4 KB)
  *     The boot block is the first block in the filesystem
  *     and contains information about the number of dir entries,
  *     inodes, and data blocks, followed by the dir entries themselves
  */
typedef struct fs_info_struct {
    uint32_t    num_dir_entries;                // number of dentrys in boot block
    uint32_t    num_inodes;                     // number of index nodes (N)
    uint32_t    data_blocks;                    // number of data nodes (D)
    uint8_t     reserved[BOOT_BLOCK_RESERVED];  // reserved padding (52 B)
    dentry_t    dir_entries[MAX_DENTRY];        // dentry items
} fs_info_struct;

/**
 * Struct for inode blocks (size = 4 KB)
 *     Inodes contain the length of the associated file
 *     followed by a list of indices of the data blocks
 *     containing the file data. Data blocks may not be
 *     sequential, and the whole inode may not be full.
 */
typedef struct inode_struct {
    uint32_t    size;                           // length of file, in bytes
    uint32_t    data_blocks[DBLOCKS_PER_INODE]; // data block indices
} inode_t;

/**
 * Struct for data blocks (size = 4 KB)
 *     Data blocks contain the raw data of a file.  Data beyond
 *     the end of the file is invalid and should not be used.
 */
typedef struct data_block_struct {
    uint8_t     data[_4KB];             // pad data_block_t to be 4 KB in size
} data_block_t;

/* Global Filesystem Variables */
uint32_t        fs_start_address;       // Address of start of filesystem
fs_info_struct* boot_block;             // Pointer to boot_block structure
inode_t*        inodes_address;         // Pointer to first inode
data_block_t*   datablocks_address;     // Pointer to first data block

/* Filesystem Utility Functions */
void filesystem_init(uint32_t filesys_addr_input);
int32_t read_dentry_by_name(const uint8_t * fname, dentry_t * dentry);
int32_t read_dentry_by_index(const uint32_t index, dentry_t * dentry);
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t * buf, uint32_t length);
/* File System Call Handlers */
int32_t file_read(int32_t fd, void* buf, int32_t nbytes);
int32_t file_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t file_open(const uint8_t* filename);
int32_t file_close(int32_t fd);
/* Directory System Call Handlers */
int32_t dir_read(int32_t fd, void* buf, int32_t nbytes);
int32_t dir_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t dir_open(const uint8_t* filename);
int32_t dir_close(int32_t fd);

#endif /* _FILESYSTEM_H */
