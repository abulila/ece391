/* cursor.c - Functions used in blinking cursor on the screen
 * tabsize:4, soft tabs
 * Source: Osdev Wiki - https://wiki.osdev.org/Text_Mode_Cursor
 */

#include "cursor.h"


/*
 * Cursor Enable Function
 *
 * Description:
 *      Enables Cursor on the screen
 * Inputs:
 *      cursor_start: starting scanline of screen
 *      cursor_end: ending scanline of the screen
 * Outputs:
 *      none
 * Side Effects:
 *      Draws blinking box cursor to the screen
 */
void enable_cursor(uint8_t cursor_start, uint8_t cursor_end)
{
    // Enable cursor from Initial Scanline
    outb(START_REG, CURSOR_PORT);
    outb((inb(CURSOR_DATA) & CURSOR_START_EN) | cursor_start, CURSOR_DATA);

    // Disable cursor from End Scanline
    outb(END_REG, CURSOR_PORT);
    outb((inb(CURSOR_DATA) & CURSOR_END_DIS) | cursor_end, CURSOR_DATA);
}

/*
 * Cursor Disable Function
 *
 * Description:
 *      Disables Cursor on the screen
 * Inputs:
 *      none
 * Outputs:
 *      none
 * Side Effects:
 *      Removes cursor from the screen
 */
void disable_cursor()
{
    // Disable Cursor
    outb(START_REG, CURSOR_PORT);
    outb(CURSOR_DIS, CURSOR_DATA);
}

/*
 * Update Cursor Location Function
 *
 * Description:
 *      Moves cursor on the screen
 * Inputs:
 *      x - X location to draw cursor
 *      y - Y location to draw cursor
 * Outputs:
 *      none
 * Side Effects:
 *      Draws cursor on the screen
 */
void update_cursor(int x, int y)
{
    uint16_t pos = y * SCREEN_WIDTH + x;   //get screen position

    // write to LOW and then HIGH location registers
    outb(LOC_LO_REG, CURSOR_PORT);
    outb((uint8_t) (pos & LOW_BYTE_MASK), CURSOR_DATA);

    //shift over one byte to extract higher byte and then write it
    outb(LOC_HI_REG, CURSOR_PORT);
    outb((uint8_t) ((pos >> 8) & LOW_BYTE_MASK), CURSOR_DATA);
}
