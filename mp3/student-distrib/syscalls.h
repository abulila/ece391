#ifndef _SYSCALLS_H
#define _SYSCALLS_H

#include "lib.h"
#include "x86_desc.h"

#define MAX_FILENAME_LENGTH             32
#define PROGRAM_VIRT_ADDR       0x08048000
#define USER_VID_MEM_ADDR       0x084B8000
#define MAX_INT_32              0xFFFFFFFF

#define PAGE_BOTTOM             0x083FFFFC

#define MAX_PROCESSES                    6
#define PROCESS_INACTIVE                 0
#define PROCESS_ACTIVE                   1

#define PDE_4MB_USR_RW_P_FLAGS        0x87

extern uint8_t pid_array[MAX_PROCESSES];

/* System Call prototypes.  Defined in syscalls.c and called
 * via an assembly linkage in interrupts.S
 */
int32_t halt(uint8_t status);
int32_t execute(const uint8_t* command);
int32_t read(int32_t fd, void* buf, int32_t nbytes);
int32_t write(int32_t fd, const void* buf, int32_t nbytes);
int32_t open(const uint8_t* filename);
int32_t close(int32_t fd);
int32_t getargs(uint8_t* buf, int32_t nbytes);
int32_t vidmap(uint8_t** screen_start);
int32_t set_handler(int32_t signum, void* handler_address);     // EXTRA CREDIT
int32_t sigreturn(void);                                        // EXTRA CREDIT

int32_t get_next_process_number();

#endif /* _SYSCALLS_H */
