/* terminal.c - Functions used in terminal driver
 * tabsize:4, soft tabs
 */
#include "terminal.h"

// #define TERMINAL_COLOR 0x3

/*
 * Terminal Open
 *
 * Description: Open a new terminal and return a file descriptor for it.
 */
int32_t terminal_open(const uint8_t* filename) {
    clear_buffer(TERM_KEY_BUF, TERM_KEY_BUF_SIZE);
    return 0;
}

/*
 * Terminal Close
 *
 * Description: Close the terminal with file descriptor fd
 * Inputs: fd - file descriptor
 * Outputs: Returns 0
 */
int32_t terminal_close(int32_t fd) {
    return 0;
}

/*
 * Terminal Read
 *
 * Description: Read a null terminated buffer from the keyboard to buffer `buf`.
 * Inputs:  fd - unused
 *          buf - pointer to buffer to receive data
 *          nbytes - size of receiving buffer
 * Outputs: Returns number of bytes read
 * Side Effects: Modifies contents of `buf`
 */
int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes) {
    int8_t* buffer;
    // If there is nothing to copy to, return error.
    if (buf == NULL) {
        return -1;
    }

    if (nbytes > TERM_KEY_BUF_SIZE) {
        nbytes = TERM_KEY_BUF_SIZE; // We limit nbytes to max size of buffer
    }

    while(1){   // loop until we have something in the buffer
        if (TERM_KEY_BUF[0] != '\0') { // if(strlen>0)
            break;
        }
    }
    buffer = (int8_t*)buf;

    // copy at most nbytes from KEY_BUF to buf,
    // stopping on a null or when buf is full
    strncpy(buffer, TERM_KEY_BUF, nbytes);

    // Clear buffer for next line
    clear_buffer(TERM_KEY_BUF, TERM_KEY_BUF_SIZE);

    // return number of bytes written to buf
    if (buffer[nbytes-1] != '\0') {
        return nbytes;
    }
    return strlen(buffer);
}

/*
 * Terminal Write
 *
 * Description: Output a null-terminated string of characters to the terminal from buffer `buf`.
 * Inputs:  fd - unused
 *          buf - pointer to buffer of data to print
 *          nbytes - amount of data to print
 * Outputs: Returns number of bytes printed
 * Side Effects: Prints characters to the screen
 */
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes) {
    uint32_t i;
    uint8_t* buffer;

    if (buf == NULL) {
        return -1;
    }

    buffer = (uint8_t*)buf;

    // print characters from buf to terminal until the
    // end of buf or a null character is found
    for (i = 0; i < nbytes; i++) {
        cputc(CUSTOM_COLORS[2], buffer[i]); // Terminal Color
    }

    // return the number of bytes printed
    return i;
}
