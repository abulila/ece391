#include "lib.h"
#include "types.h"
#include "x86_desc.h"

#include "filesystem.h"
#include "pcb.h"
#include "rtc.h"
#include "syscalls.h"
#include "terminal.h"
#include "tests.h"

#define PASS 1
#define FAIL 0

/* format these macros as you see fit */
#define TEST_HEADER     \
    printf("[TEST %s] Running %s at %s:%d\n", __FUNCTION__, __FUNCTION__, __FILE__, __LINE__);
#define TEST_OUTPUT(name, result)   \
    printf("[TEST %s] Result = %s\n\n", name, (result) ? "PASS" : "FAIL");

static inline void assertion_failure(){
    /* Use exception #15 for assertions, otherwise
       reserved by Intel */
    asm volatile("int $15");
}

#define wait_and_clear()        \
do {                            \
    cli();                      \
    int count = 0;              \
    while (count < 500000000)   \
        count++;                \
    clear();                    \
    sti();                      \
} while(0);


//------------------------------------------------------------------------------
//  Checkpoint 1 tests
//------------------------------------------------------------------------------

/* IDT Test - Example
 *
 * Asserts that first 10 IDT entries are not NULL
 * Inputs: None
 * Outputs: PASS/FAIL
 * Side Effects: None
 * Coverage: Load IDT, IDT definition
 * Files: x86_desc.h/S
 */
int idt_test(){
    TEST_HEADER;

    int i;
    int result = PASS;
    for (i = 0; i < 10; ++i){
        if ((idt[i].offset_15_00 == NULL) &&
            (idt[i].offset_31_16 == NULL)){
            assertion_failure();
            result = FAIL;
        }
    }

    return result;
}

/* Exception - Divide by zero
 *
 * Tests divide by zero exception handling
 * Inputs: None
 * Outputs: Divide Error/FAIL
 * Side Effects: None
 * Coverage: IDT entry, exception handler
 * Files: idt.c
 */
int idt_test_division_by_zero() {
    TEST_HEADER;
    int zero = 0; /* Separate variable to avoid compiler warning */
    int division = NONZERO_TEST_VALUE/zero;

    division++; /* Remove unused variable compiler warning */
    // if we made it here, the exception failed
    assertion_failure();

    return FAIL;
}

/* Paging valid memory access
 *
 * Asserts that the video memory is accessible.
 * Inputs: None
 * Outputs: PASS/PAGEFAULT
 * Side Effects: None
 * Coverage: Paging valid memory access
 * Files: paging.c/h
 */
int paging_valid_mem_test(){
    TEST_HEADER;

    int vid,i;
    int result = PASS;
    int * vidmem = (int *)VID_MEM_ADDR;
    for(i = 0; i < VID_MEM_4KB_BY_4B_MAX ; i++){
        vid = vidmem[i];
    }
    return result; /* Expecting a page fault in the case of failure */
}

/* Paging invalid memory access through NULL dereference
 *
 * Asserts that the NULL pointer is inaccessible.
 * Inputs: None
 * Outputs: PAGEFAULT/FAIL
 * Side Effects: None
 * Coverage: Paging invalid memory access denial
 * Files: paging.c/h
 */
int paging_invalid_mem_null_test(){
    unsigned int * arbitrary;
    unsigned int arbitrary_input;
    arbitrary = NULL;
    arbitrary_input = *arbitrary;

    return FAIL; /* Returns FAIL because it expects a page fault */
}

/* Paging invalid memory access through non-present page in first PDE
 *
 * Asserts that the memory right before video memory is inaccessible.
 * Inputs: None
 * Outputs: PAGEFAULT/FAIL
 * Side Effects: None
 * Coverage: Paging invalid memory access denial
 * Files: paging.c/h
 */
int paging_invalid_mem_first_4mb_test(){
    unsigned int * arbitrary;
    unsigned int arbitrary_input;
    arbitrary =(unsigned int *) PAGING_INVALID_FIRST4MB_ADDR;
    arbitrary_input = *arbitrary;

    return FAIL; /* Returns FAIL because it expects a page fault */
}

/* Paging invalid memory access through memory beyond current use dereference
 *
 * Asserts that the memory not currently present beyond the first 8MB is inaccessible.
 * Inputs: None
 * Outputs: PAGEFAULT/FAIL
 * Side Effects: None
 * Coverage: Paging invalid memory access denial
 * Files: paging.c/h
 */
int paging_invalid_mem_beyond_test(){
    unsigned int * arbitrary;
    unsigned int arbitrary_input;
    arbitrary = (unsigned int *)PAGING_INVALID_OVER_8MB_ADDR;
    arbitrary_input = *arbitrary;

    return FAIL; /* Returns FAIL because it expects a page fault */
}

/* Paging valid memory access at 2nd 4MB kernel memory
 * NOTE: Meant to be run by a kernel level program which is allowed to access said memory.
 *  Additionally, tests initial bound of valid memory access.
 *  Asserts that the kernel memory is accessible.
 * Inputs: None
 * Outputs: PASS/PAGEFAULT
 * Side Effects: None
 * Coverage: Paging valid memory access
 * Files: paging.c/h
 */
int paging_attempt_kernel_access(){
    unsigned int * arbitrary;
    unsigned int arbitrary_input;
    arbitrary = (unsigned int *)KERNEL_ACCESS_ADDR;
    arbitrary_input = *arbitrary;

    return PASS; /* Returns PASS because failure is a page fault */
}

/* Paging invalid memory access through memory beyond current use dereference
 *
 * Asserts that the memory not currently present beyond the first 8MB is inaccessible.
 * Inputs: None
 * Outputs: PAGEFAULT/FAIL
 * Side Effects: None
 * Coverage: Paging invalid memory access denial
 * Files: paging.c/h
 */
int paging_edge_of_vid_mem(){
    int vid;
    int result = FAIL;
    int * vidmem = (int *)VID_MEM_ADDR;
    vid = vidmem[VID_MEM_4KB_BY_4B_MAX-1]; /* -1 keeps it just within page limits */
    printf("CAN SUCCESSFULLY ACCESS EDGE                    \n"); /* Prints to make clear successful initial access */
    vid = vidmem[VID_MEM_4KB_BY_4B_MAX]; /* Access right beyond page limits */

    return result; /* Returns FAIL because it expects a page fault */
}

//------------------------------------------------------------------------------
//  Checkpoint 2 tests
//------------------------------------------------------------------------------

#define NUM_COLS 80

#define TEST_BUF_SIZE 391
/* Defines for filesystem_dir_read_test */
// Buffer Lengths
#define FNAME_BUF_LEN (FILENAME_SIZE+1)
#define FNAME_SPACE_LEN FILENAME_SIZE
#define FSIZE_BUF_LEN 10
#define FSIZE_SPACE_LEN 6

#define BUF_SIZE sizeof(dentry_t)
#define NUM_FILES 17
#define DUMMY_BUF_SIZE 2
#define BYTES_32KB (_4KB*8)

// Buffer Declarations
int8_t FNAME_BUF[FNAME_BUF_LEN];
int8_t FNAME_SPACE[FNAME_SPACE_LEN];
int8_t FSIZE_BUF[FSIZE_BUF_LEN];
int8_t FSIZE_SPACE[FSIZE_SPACE_LEN];

/* RTC driver test
*
* Tests that the RTC driver is working by printing characters at various frequencies
* Inputs: None
* Outputs: PASS (check screen for output)
* Side Effects: clears screen and outputs multiple lines of text
* Coverage: RTC driver
* Files: rtc.{c,h}
*/
int rtc_driver_test() {
    uint32_t fd;
    uint32_t freq;
    uint32_t i, j;

    int8_t buf[NUM_COLS] = {0};
    int8_t dot = '.';
    int8_t sp = ' ';
    int8_t lf = '\n';

    TEST_HEADER;

    fd = rtc_open(NULL);
    // loop 10 times, once for each RTC frequency
    for (i = 1; i <= 10; i++) {
        freq = (1<<i);  // start with 2 Hz and increment every 5 seconds
        strcpy(buf, "Freq: ");
        itoa(freq, &(buf[6]), 10);
        terminal_write(0, buf, strlen(buf));
        terminal_write(0, &sp, 1);

        rtc_write(fd, &freq, 4);    // write the frequency

        // print for the lesser of 60 characters or 3 seconds
        j = (60 < freq*3) ? 60 : freq*3;
        for (; j > 0; j--) {
            rtc_read(fd, NULL, 0);  // read 0 bytes (wait for interrupt)
            terminal_write(0, &dot, 1);
        }

        terminal_write(0, &lf, 1);
    }

    rtc_close(fd);

    return PASS;
}

/* Terminal Read Test
 *
 * Tests terminal read can read from the keyboard correctly
 * Inputs: None
 * Outputs: PASS (check screen for output)
 * Side Effects: clears screen and echoes keyboard strings when ENTER is pressed
 * Coverage: Terminal Driver
 * Files: terminal.c/h, keyboard.c/h
 */
int terminal_read_test() {
    uint32_t fd;
    uint32_t n;
    int8_t buf[TEST_BUF_SIZE] = {0};  // character buf
    int8_t echo[4] = ">> "; // Set echo string
    fd = terminal_open(NULL);

    // print headers
    TEST_HEADER;
    strcpy(buf, "Press CTRL+C to exit.\n");
    terminal_write(0, buf, strlen(buf));

    n = terminal_read(0, buf, sizeof(buf)); // terminal read
    if(n == -1){
        terminal_close(fd);
        return FAIL;    // error case
    }
    terminal_write(0, echo, sizeof(echo));  // echo to screen
    terminal_write(0, buf, n);    // clear buffer
    // print information of read
    printf("  << Size of Buf: %d, Bytes Read: %d >>\n", sizeof(buf),n);

    terminal_close(fd);
    return PASS;
}

/* Filesystem Index Test:
 *
 * Description: Iterate over dentrys by index, printing
 *              the filename if present
 * Inputs: None
 * Outputs: PASS
 * Side Effects: prints a list of all files to the screen
 * Coverage: read_dentry_by_index
 * Files: filesystem.{c,h}
 */
int read_files_by_index() {
    dentry_t dentry;

    int i; // iteration variable
    /*
     * Go through each dentry, and
     * for `dentry`s with non-null names,
     * copy and print the filename.
     */
    for (i = 0; i < MAX_DENTRY; i++) {
        read_dentry_by_index(i, &dentry);
        if (dentry.filename[0] != '\0') {
            strncpy(FNAME_BUF, (int8_t*)dentry.filename, FILENAME_SIZE);
            FNAME_BUF[FNAME_BUF_LEN-1] = '\0';
            printf("Index %d: %s\n", i, FNAME_BUF);
        }
    }

    return PASS;
}

/* Filesystem dir_read Test:
 *
 * Description: Read through the directory and print each file's
 *              filename, filetype and size to the screen.
 * Inputs: None
 * Outputs: PASS
 * Side Effects: prints a list of all files and their info to the screen
 * Coverage: dir_open, dir_read, read_dentry_by_name
 * Files: filesystem.{c,h}
 */
int filesystem_dir_read_test() {
    // setup variables
    int ret_val, file_space;
    dentry_t dentry;
    uint8_t buf[BUF_SIZE];
    dir_open((uint8_t*)".");

    // dir read "."
    ret_val = dir_read(0, buf, BUF_SIZE);

    // while there are entries: loop
    while (ret_val != 0) {
        // clear print format buffers
        memset(FNAME_SPACE, '\0', FNAME_SPACE_LEN);
        memset(FSIZE_SPACE, '\0', FSIZE_SPACE_LEN);

        // read dentry
        read_dentry_by_name(buf, &dentry);

        // Set file name buffer and spaces
        strncpy(FNAME_BUF, (int8_t*)dentry.filename, FILENAME_SIZE);
        FNAME_BUF[FNAME_BUF_LEN-1] = '\0';
        memset(FNAME_SPACE, ' ', FNAME_SPACE_LEN-strlen(FNAME_BUF));

        // Set file size buffer and spaces
        file_space = (inodes_address+dentry.inode_number)->size;  // get size of file
        // file_space = *((uint32_t*)(fs_start_address+(dentry.inode_number+1)*BYTES_4KB));
        itoa(file_space, FSIZE_BUF, FSIZE_BUF_LEN);
        FSIZE_BUF[FSIZE_BUF_LEN-1] = '\0';
        memset(FSIZE_SPACE, ' ', FSIZE_SPACE_LEN-strlen(FSIZE_BUF));

        // formatted output to screen
        printf("file_name: %s%s,  file_type: %d,  file_size: %s%s\n", FNAME_SPACE, FNAME_BUF, dentry.filetype, FSIZE_SPACE, FSIZE_BUF);

        // get next element
        ret_val = dir_read(0, buf, BUF_SIZE);
    }

    // close file and open
    dir_close(0);
    return PASS;
}

/* Filesystem Read File by Name Test:
 *
 * Description: Find the file matching the provided filename and
 *              print its contents to the screen.
 * Inputs: fname - filename of file to print
 * Outputs: PASS
 * Side Effects: prints contents of file to the screen
 * Coverage: read_dentry_by_name, file_open, file_read
 * Files: filesystem.{c,h}
 */
int test_file_by_name(uint8_t* fname) {
    dentry_t dentry;
    uint8_t buf[BYTES_32KB];
    int i = 0; // iteration variable
    uint32_t num_read;

    // open file for reading and read dentry by given fname
    file_open(fname);
    read_dentry_by_name(fname, &dentry);
    num_read = file_read(0, buf, BYTES_32KB);
    file_close(0);

    // echo the contents of the buf to screen char by char
    while (i < num_read) {
        putc(buf[i]);
        i++;
    }

    // print the filename at the end
    printf("\n\nfile_name: %s\n", fname);
    return PASS;
}

/* Filesystem Read Files Test:
 *
 * Description: Find the files in the filesystem and
 *              print its contents to the screen.
 * Inputs: none
 * Outputs: PASS
 * Side Effects: prints contents of the files to the screen
 * Coverage: read_dentry_by_name, file_open, file_read
 * Files: filesystem.{c,h}
 */
int test_files() {
    uint8_t dummy_buf[DUMMY_BUF_SIZE]; // generate a buffer to handle character presses

    // list of all files in the filesystem to test
    int8_t* fnames[NUM_FILES] = {
        ".",            "sigtest",      "shell",       "grep",
        "syserr",       "rtc",          "fish",        "counter",
        "pingpong",     "cat",          "frame0.txt",
        "verylargetextwithverylongname.txt",
        "ls",           "testprint",    "created.txt",
        "frame1.txt",   "hello"
    };

    uint8_t* fname;
    int i; // iteration variables

    /*
     * go through each file, opening each and echoing
     * each file to the screen char by char
     */
    for (i = 0; i < NUM_FILES; i++) {
        fname = (uint8_t*) fnames[i];
        test_file_by_name(fname);
        printf("Press enter to continue...\n");
        terminal_read(0, dummy_buf, sizeof(dummy_buf));
    }

    return PASS;
}

/* Mother of All Filesystem Tests
 *
 * Tests every aspect of the filesystem at once.
 *  - First, this test will iterate over the boot block by
 *    calling read_dentry_by_index() and printing the values
 *    stored in each valid dentry.
 *  - Second, this test scans the boot block using dir_open()
 *    and dir_read() to get a list of file names, each of which
 *    is printed with their corresponding file type and size,
 *    using read_dentry_by_name() to look up file info.
 *  - Third, this test will sequentially "cat" each file previously
 *    read by dir_read, outputting the contents to the terminal
 *    using file_open(), file_read(), and file_close().
 *
 * Inputs: None
 * Outputs: PASS (check screen for correct output)
 * Coverage: Filesystem Driver
 * Files: filesystem.c/.h
 */
int moa_filesystem_tests() {
    uint8_t dummy_buf[2];

    TEST_HEADER;
    printf("Press enter to begin...\n");
    terminal_read(0, dummy_buf, sizeof(dummy_buf));
    {
        read_files_by_index();
    }

    printf("\nPart 1 Complete. Press enter to continue...\n");
    terminal_read(0, dummy_buf, sizeof(dummy_buf));
    {
        filesystem_dir_read_test();
    }

    printf("\nPart 2 Complete. Press enter to continue...\n");
    terminal_read(0, dummy_buf, sizeof(dummy_buf));
    {
        test_files();
    }

    printf("\nPart 3 Complete\n");
    return PASS;
}


//------------------------------------------------------------------------------
//  Checkpoint 3 tests
//------------------------------------------------------------------------------

/* System Call Open/Read/Close Test (via Interrupt):
 *
 * Description: Uses interrupt 0x80 to call system open()
 *              and read() functions on a file, then prints
 *              the file contents using putc.
 * Inputs: none
 * Outputs: PASS/FAIL
 * Side Effects: prints contents of the files to the screen
 * Coverage: interrupt 0x80, system call read/open/close
 * Files: interrupts.S, syscalls.{c,h}
 */
int test_open_read_int() {
    pcb_t* pcb_ptr = get_pcb_ptr();
    pcb_init_fd(pcb_ptr);

    // open file
    char* filename = "frame0.txt";
    int fd;
    asm volatile("              \n\
            movl $5, %%eax      \n\
            movl %1, %%ebx      \n\
            int $0x80           \n\
            "
            : "=a" (fd)     // EAX is syscall return, save fd #
            : "m"(filename)
    );
    if (fd < 0) {
        printf ("Failed to open file: %s\n", filename);
        return FAIL;
    }
    printf("File %s opened with fd %d\n", filename, fd);

    // read file contents
    int buffer_size = 255;          // arbitrary value larger than file size
    char buffer[buffer_size+1];     // buffer for file data
    buffer[buffer_size] = '\0';     // ensure it is null terminated
    int nbytes;                     // bytes read from file
    char* buff_ptr = buffer;        // required for inline asm
    asm volatile("              \n\
            movl $3, %%eax      \n\
            movl %2, %%ebx      \n\
            movl %1, %%ecx      \n\
            movl %3, %%edx      \n\
            int $0x80           \n\
            "
            : "=a" (nbytes), "+m" (buff_ptr)
            : "rm" (fd), "rm" (buffer_size)
    );
    printf("Read %d bytes from %s\n", nbytes, filename);

    // close file descriptor
    int rv;
    asm volatile("              \n\
            movl $6, %%eax      \n\
            movl %1, %%ebx      \n\
            int $0x80           \n\
            "
            : "=a" (rv)
            : "rm" (fd)
    );
    if (rv == -1) {
        printf("Close fd %d failed\n", fd);
        return FAIL;
    }

    // print file contents
    int i = 0;
    char c;
    while((c = buffer[i++]) != '\0') {
        putc(c);
    }

    return PASS;
}

/* System Call Open/Read/Close Test (direct call):
 *
 * Description: Reads a file in a single chunk using system
 *              open() and read() then prints the contents
 *              using putc().  File is closed via system close().
 * Inputs: none
 * Outputs: PASS/FAIL
 * Side Effects: prints contents of the files to the screen
 * Coverage: system call read/open/close
 * Files: syscalls.{c,h}
 */
int test_open_read_direct() {
    TEST_HEADER;

    pcb_t* pcb_ptr = get_pcb_ptr();
    pcb_init_fd(pcb_ptr);

    char* filename = "frame0.txt";
    int fd;
    if ((fd = open((uint8_t*)filename)) == -1) {
        printf("Error: failed to open file\n");
        return FAIL;
    }

    int nbytes;
    int buffer_size = 256;  // arbitrary value larger than file size
    char buffer[buffer_size];
    buffer[buffer_size - 1] = '\0';
    if ((nbytes = read(fd, buffer, buffer_size)) == -1) {
        printf("Error: failed to read file\n");
        return FAIL;
    }

    close(fd);

    printf("Read %d bytes from %s\n", nbytes, filename);

    int i = 0;
    char c;
    // null check is ok because we are looking at a text file
    while((c = buffer[i++]) != '\0') {
        putc(c);
    }
    return PASS;
}

/* System Call Partial Read/Write Test (direct call):
 *
 * Description: Reads a file in pieces by directly calling
 *              system open() and read() functions.  Subsequent
 *              reads should read subsequent data from the file.
 *              Each call to read prints the bytes read until the
 *              end of file is reached.  File is closed by calling close().
 * Inputs: none
 * Outputs: PASS/FAIL
 * Side Effects: prints contents of the files to the screen
 * Coverage: system call read/open/close
 * Files: syscalls.{c,h}
 */
int test_partial_read_write_direct() {
    TEST_HEADER;

    pcb_t* pcb_ptr = get_pcb_ptr();
    pcb_init_fd(pcb_ptr);

    char* filename = "frame0.txt";
    int fd;
    if ((fd = open((uint8_t*)filename)) == -1) {
        printf("Error: failed to open file\n");
        return FAIL;
    }

    int nbytes;
    int buffer_size = 32;  // arbitrary value smaller than file size
    char buffer[buffer_size];

    // Initial read to ensure file is valid
    nbytes = read(fd, buffer, buffer_size);
    if (nbytes < 0) {
        printf("Error: failed to read file\n");
        return FAIL;
    }

    // print bytes read and continue until EOF
    int i;
    while(nbytes > 0) {
        for (i = 0; i < nbytes; i++) {
            putc(buffer[i]);
        }
        nbytes = read(fd, buffer, buffer_size);
    }

    close(fd);
    return PASS;
}

/* System Call Open/Close Test (direct call):
 *
 * Description: Tests limitations of file descriptors by attempting
 *              to open more files than available file descriptors.
 *              This then tests closing each open file, including
 *              the directory.
 * Inputs: none
 * Outputs: PASS/FAIL
 * Side Effects: none
 * Coverage: system call open/close
 * Files: syscalls.{c,h}
 */
int test_open_close_direct() {
    TEST_HEADER;

    pcb_t* pcb_ptr = get_pcb_ptr();
    pcb_init_fd(pcb_ptr);

    char* dirname = ".";
    int fd_dir;
    if ((fd_dir = open((uint8_t*)dirname)) == -1) {
        printf("Error: failed to open directory\n");
        return FAIL;
    }
    printf("Opened %s with fd %d\n", dirname, fd_dir);

    int fname_size = FILENAME_SIZE;
    char fname[fname_size + 1];
    fname[fname_size] = '\0';

    // open the first 5 files
    int i;
    int fd[5];  // array of file descriptors
    int nbytes;
    for (i = 0; i < 5; i++) {
        nbytes = read(fd_dir, fname, fname_size);
        // skip reopening the directory
        if (0 == strncmp(fname, ".", fname_size)) {
            i--;    // modify i to avoid counting this directory
            continue;
        }

        fd[i] = open((uint8_t*)fname);
        if (fd[i] >= 0) {
            printf("Opened %s with fd %d\n", fname, fd[i]);
        }
        else {
            printf("Failed to open %s\n", fname);
            return FAIL;
        }
    }

    // try to open 1 more file
    nbytes = read(fd_dir, fname, fname_size);
    int fd_err = open((uint8_t*)fname);
    if (fd_err >= 0) {
        printf("Successfully opened too many files: %s opened with fd %d\n", fname, fd_err);
        return FAIL;
    }
    else {
        printf("File open failed successfully\n");
    }

    int rv;
    for (i = 0; i < 5; i++) {
        rv = close(fd[i]);
        if (rv < 0) {
            printf("Error closing file descriptor %d\n", fd[i]);
            return FAIL;
        }
    }
    close(fd_dir);

    printf("Closed all files successfully\n");

    return PASS;
}

/* System Call Read/Write Test (direct call):
 *
 * Description: Unit tests sys_io. Reads from keyboard and prints
 *              to screen.
 * Inputs: none
 * Outputs: PASS/FAIL
 * Side Effects: prints contents of the keyboard to the screen
 * Coverage: system call read/write, fd[0,1]
 * Files: syscalls.{c,h}, pcb.{c,h}
 */
int sys_io_test() {
    TEST_HEADER;

    printf("Type characters followed by ENTER to echo.\n");
    pcb_t* pcb_ptr = get_pcb_ptr();
    pcb_init_fd(pcb_ptr);

    int sys_in_fd = 0;
    int sys_out_fd = 1;
    int nbytes;
    char buffer[TEST_BUF_SIZE];
    buffer[TEST_BUF_SIZE - 1] = '\0';
    int8_t* echo = ">> "; // Set echo string


    if ((nbytes = read(sys_in_fd, buffer, TEST_BUF_SIZE)) == -1) {
        printf("Error: failed to read file\n");
        return FAIL;
    }
    write(sys_out_fd, echo, sizeof(echo));
    write(sys_out_fd, buffer, nbytes);
    return PASS;
}


//------------------------------------------------------------------------------
//  Checkpoint 4 tests
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  Checkpoint 5 tests
//------------------------------------------------------------------------------


/*
 * Launch tests
 *
 * Description:
 *      Testing harness launch point for the various tests defined above.
 *      Tests are commented/uncommented as desired and then launched from here
 *      upon kernel.c calling it in the case of the RUN_TESTS parameter
 *      being defined.
 * Inputs: None.
 * Outputs: None.
 * Side Effects:
 *      Runs desired tests and reports on success/failure.
 *      (Exception: They cause an exception.)
 */
void launch_tests(int checkpoint) {
    // launch your tests here
    clear();

    switch(checkpoint) {
        case 1: /* Checkpoint 1 Tests */
        {
            /* These tests should pass. */
            TEST_OUTPUT("idt_test", idt_test());
            TEST_OUTPUT("paging_valid_mem_test", paging_valid_mem_test());
            TEST_OUTPUT("paging_attempt_kernel_access", paging_attempt_kernel_access());

            /* These tests should result in exceptions. */
            // TEST_OUTPUT("idt_test_division_by_zero: Divide by 0", idt_test_division_by_zero());
            // TEST_OUTPUT("paging_invalid_mem_first_4mb_test", paging_invalid_mem_first_4mb_test());
            // TEST_OUTPUT("paging_invalid_mem_beyond_test", paging_invalid_mem_beyond_test());
            // TEST_OUTPUT("paging_invalid_mem_null_test", paging_invalid_mem_null_test());
            // TEST_OUTPUT("paging_edge_of_vid_mem", paging_edge_of_vid_mem());
            break;
        }
        case 2: /* Checkpoint 2 Tests */
        {
            TEST_OUTPUT("mother_of_all_filesystem_tests", moa_filesystem_tests());
            TEST_OUTPUT("rtc_driver_test", rtc_driver_test());
            TEST_OUTPUT("terminal_read_test", terminal_read_test());
            break;
        }
        case 3: /* Checkpoint 3 Tests */
        {
            TEST_OUTPUT("test_open_read_int", test_open_read_int());
            // TEST_OUTPUT("test_open_read_direct", test_open_read_direct());
            // TEST_OUTPUT("test_partial_read_write_direct", test_partial_read_write_direct());
            // TEST_OUTPUT("test_open_close_direct", test_open_close_direct());
            // TEST_OUTPUT("sys_io_test", sys_io_test());
            break;
        }
        case 4: /* Checkpoint 4 Tests */
        {
            printf("No tests for Checkpoint 4\n");
            break;
        }
        case 5: /* Checkpoint 5 Tests */
        {
            printf("No tests for Checkpoint 5\n");
            break;
        }
        default:
        {
            printf("Invalid Checkpoint: %d\n", checkpoint);
            return;
        }
    }

    printf("Testing Checkpoint %d Finished\n", checkpoint);
}
