#ifndef _PCB_H
#define _PCB_H

#include "types.h"
#include "terminal.h"
#include "rtc.h"
#include "filesystem.h"

#define PCB_PTR_MASK  0xFFFFE000

#define MAX_FD                 8       /* Maximum number of file descriptors   */
#define STDIN_FILENO           0       /* File descriptor number for stdin     */
#define STDOUT_FILENO          1       /* File descriptor number for stdout    */
#define SEEK_SET               0       /* Seek value for start of file         */

#define IN_USE                 1       /* File descriptor in use               */
#define NOT_IN_USE             0       /* File descriptor not in use           */

// #define CMD_BUF_LEN 128      // change to keyboard buf len or something later

/**
 * Struct for file operations jump table
 */
typedef struct fops_jumptable {
    int32_t (*read)(int32_t fd, void* buf, int32_t nbytes);
    int32_t (*write)(int32_t fd, const void* buf, int32_t nbytes);
    int32_t (*open)(const uint8_t* filename);
    int32_t (*close)(int32_t fd);
} fops_jumptable_t;

/**
 * Struct for file descriptor
 *     Contains jump table, inode number,
 *     seek position, and file flags.
 */
typedef struct file_desc {
    fops_jumptable_t*   fops_jump_table;
    int32_t             inode;
    uint32_t            seek;
    uint32_t            flags;
} file_desc_t;

/**
 * Struct for process control block
 *     Contains file descriptor array, process ID,
 *     and pointer to parent process' PCB.
 */
typedef struct pcb_struct {
    file_desc_t         fd_array[MAX_FD];
    uint32_t            pid;
    int32_t             parent_pid;
    int32_t             program_return_status;
    // TODO: these wont neccessarily store kernel data
    int32_t             save_kbp;              // save the kernel space base pointer
    int32_t             save_ksp;              // save the kernel space stack pointer
    int32_t             save_esp0;
    uint8_t             arg_buf[TERM_KEY_BUF_SIZE];     // have argument buffer of same size as terminal buffer

    uint32_t            termination_esp;
    
} pcb_t;

void pcb_init_fd(pcb_t* pcb);
pcb_t* get_pcb_ptr(void);
pcb_t* get_pcb_for_process(uint32_t pid);

int32_t file_op_invalid();


/* Define standard filetype operation jumptables */
extern fops_jumptable_t stdin_ops;
extern fops_jumptable_t stdout_ops;
extern fops_jumptable_t rtc_ops;
extern fops_jumptable_t dir_ops;
extern fops_jumptable_t file_ops;
extern fops_jumptable_t no_ops;


#endif /* _PCB_H */
