/* keyboard.h - Defines used in keyboard interrupt handler
 * tabsize:4, soft tabs
 */

#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include "i8259.h"
#include "lib.h"
#include "cursor.h"
#include "terminal.h"
#include "colors.h"

/* PS/2 Keyboard Commands and Port Numbers*/
/* From OSdev wiki: https://wiki.osdev.org/%228042%22_PS/2_Controller */

#define KEYBOARD_IRQ    1       // Keyboard is IRQ1 on PIC
#define PS2_DATA_PORT   0x60    // R/W Data Port
#define PS2_STATUS_REG  0x64    // Read Only Status Register
#define PS2_CMD_REG     0x64    // WOnly Command Register
#define ENABLE1_CMD     0xAE    // Enable first PS/2 Port

#define NUM_KEYS        96      // Size of Look up Table

#define CURSOR_START    0       // min scanline for cursor
#define CURSOR_END      24      // max scanline for cursor

// Keyboard Buffer Defines
#define KEY_BUF_SIZE    128
#define MAX_INDEX       127
int8_t KEY_BUFFER[KEY_BUF_SIZE];
uint8_t BUF_IDX;                // Cursor index in buffer


/* Functions */
/* Initialize Keyboard */
void keyboard_init();
/* Keyboard Interrupt Handler */
void keyboard_interrupt();
/* Clear Given char Buffer */
void clear_buffer(int8_t* buf, uint32_t size);

#endif /* _KEYBOARD_H */
