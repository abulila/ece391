/* terminal.h - Defines used in terminal driver
 * tabsize:4, soft tabs
 */

#ifndef _TERMINAL_H
#define _TERMINAL_H

#include "keyboard.h"
#include "colors.h"

/* System Call Handlers */
int32_t terminal_open(const uint8_t* filename);
int32_t terminal_read(int32_t fd, void* buf, int32_t nbytes);
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t terminal_close(int32_t fd);

/* Buffer defines */
#define TERM_KEY_BUF_SIZE 128
int8_t TERM_KEY_BUF[TERM_KEY_BUF_SIZE];

#endif /* _TERMINAL_H */
