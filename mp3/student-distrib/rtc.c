#include "lib.h"
#include "i8259.h"
#include "rtc.h"

/* Local helper functions */
static int8_t rtc_set_freq(uint32_t freq);
// flag for detecting when interrupts are called
volatile uint8_t rtc_interrupt_occurred = 0;

/*
 * RTC Initialization
 *
 * Description:
 *      Initialize the RTC to generate periodic interrupts at
 *      an initial frequency of 2 Hz.
 * Inputs: None
 * Outputs: None
 * Side Effects: Initializes RTC
 */
void rtc_init(void) {
    uint8_t old_a, old_b;   // original values of crA and crB
    uint8_t temp;   // temp value for writing to registers

    // Read the previous values of crA and crB
    outb(RTC_CRA, RTC_PORT);
    old_a = inb(RTC_CMOS);
    outb(RTC_CRB, RTC_PORT);
    old_b = inb(RTC_CMOS);

    // Set Control Register A value
    //   Maintain bit 7 while clearing all others
    //   Set DV to enable the oscillator (bits 6:4)
    //   Set RS to the desired frequency (bits 3:0)
    temp = (old_a & MASK_7) | SET_DV | freq2;
    outb(RTC_CRA, RTC_PORT);
    outb(temp, RTC_CMOS);

    // Set Control Register B
    //   Maintain bits 3:0
    //   Clear SET, AIE, UIE (bits 7, 5, 4)
    //   Set PIE (bit 6)
    temp = (old_b & MASK_LOW) | SET_PIE;
    outb(RTC_CRB, RTC_PORT);
    outb(temp, RTC_CMOS);

    // RTC initialized, enable the interrupt line
    enable_irq(RTC_IRQ);
}

/*
 * RTC Interrupt Service Routine
 *
 * Description:
 *      Function to run when RTC triggers and interrupt.
 * Inputs: None
 * Outputs: None
 * Side Effects:
 *      Signals EOI to PIC
 */
void rtc_interrupt(void) {
    cli();

    // Read crC to clear the IRQF
    // We don't actually need the value
    outb(RTC_CRC, RTC_PORT);
    inb(RTC_CMOS);

    // Tell the PIC that we are done
    send_eoi(RTC_IRQ);

    // set the flag so we know an interrupt happened
    rtc_interrupt_occurred = 1;

    // printf("RTC interrupt occured\n");
    // test_interrupts();

    sti();
}


/*
 * RTC Open
 *
 * Description: Open a new file descriptor for the RTC device
 * Inputs:
 * Outputs: Returns 0
 * Side Effects: Sets RTC frequency to 2 Hz
 */
int32_t rtc_open(const uint8_t* filename) {
    // squash compiler warning about unused variable
    (void)filename;

    // set the RTC frequency to 2 Hz
    // (ignore the return value because we know it is valid)
    (void)rtc_set_freq(2);

    // Always return 0
    return 0;

}


/*
 * RTC Read
 *
 * Description: Wait for the RTC to raise an interrupt and return.
 * Inputs: ignored
 * Outputs: Returns 0 after next RTC interrupt
 */
int32_t rtc_read(int32_t fd, void* buf, int32_t nbytes) {
    // squash compiler warning about unused variable
    (void)fd;
    (void)buf;
    (void)nbytes;

    // clear the flag so we can detect the next interrupt
    cli();
    rtc_interrupt_occurred = 0;
    sti();

    // wait for the next interrupt to occur
    while(!rtc_interrupt_occurred); // spin

    // always return 0
    return 0;
}


/*
 * RTC Write
 *
 * Description: Set the RTC periodic interrupt frequency.  Input must be
 *              a 4-byte integer and a power of 2 between 2 and 1024 Hz (inclusive).
 * Inputs: buf - pointer to input frequency (4 byte integer)
 *         nbytes - size of buf, must be 4
 * Outputs: Returns -1 on failure or number of bytes written (4) on success
 * Side Effects: Changes the RTC interrupt frequency.
 */
int32_t rtc_write(int32_t fd, const void* buf, int32_t nbytes) {
    int32_t freq;
    int8_t status;

    // ensure we have 4 bytes of non-null memory to write
    if (nbytes != 4 || buf == NULL) {
        return -1;
    }

    // cast buf to an int pointer and
    // deference to get the frequency
    freq = *(int32_t*)(buf);

    // Attempt to set the specified frequency.
    // If the write fails (freq is not a power of 2
    // or is out of range), return -1, otherwise
    // return 4 (# bytes written).
    status = rtc_set_freq(freq);
    return (status < 0) ? -1 : 4;
}


/*
 * RTC Close
 *
 * Description: Close the file descriptor for the RTC.
 * Inputs: fd - file descriptor
 * Outpus: Returns 0
 */
int32_t rtc_close(int32_t fd) {
    // squash compiler warning about unused variable
    (void)fd;

    return 0;
}


/*
 * RTC Frequency Set
 *
 * Description: Change the frequency of the RTC periodic interrupts. Frequency
 *              must be a power of 2 between 2 and 1024, inclusive.
 * Inputs: freq - new frequency
 * Outputs: Returns 0 on success, -1 on failure
 * Side Effects: Changes RTC interrupt frequency.
 */
int8_t rtc_set_freq(uint32_t freq) {
    uint8_t out;
    uint8_t old_a;

    // Select the appropriate byte value for the
    // desired frequency.  Frequencies must be a
    // power of 2 between 2 and 1024, inclusive.
    // If the frequency is invalid, return -1.
    switch (freq) {
        case 2:     out = freq2;     break;
        case 4:     out = freq4;     break;
        case 8:     out = freq8;     break;
        case 16:    out = freq16;    break;
        case 32:    out = freq32;    break;
        case 64:    out = freq64;    break;
        case 128:   out = freq128;   break;
        case 256:   out = freq256;   break;
        case 512:   out = freq512;   break;
        case 1024:  out = freq1024;  break;
        default:
            return -1;
    }

    cli();      // begin critical section

    // read the current register value
    outb(RTC_CRA, RTC_PORT);
    old_a = inb(RTC_CMOS);

    // preserve bits 7:4 and update bits 3:0
    out |= (old_a & MASK_HIGH);

    // write the new value to the RTC
    outb(RTC_CRA, RTC_PORT);
    outb(out, RTC_CMOS);

    sti();      // end critical section

    // return success
    return 0;
}
