#include "colors.h"

color CUSTOM_COLORS[NUM_CUSTOM] = {
    WHITE, // Default_Clr
    WHITE, // Keyboard_Clr
    CYAN   // Terminal_Clr
};
