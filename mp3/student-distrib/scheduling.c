#include "scheduling.h"

void schedule_init() {
    // Initialize all terminals to inactive, indicating they
    // are ready to recieve their first program
    int i;
    for( i = 0; i < TERMINALS_COUNT; i++) {
        terminal_processes[i] = -1; //-1 representing no running base shell
    }

    // Start on terminal 0
    current_terminal = 0;

    // Initialize each process
    for (i = 0; i < MAX_PROCESSES; i++) {
        processes[i].save_esp = NULL;   // Empty processes don't have an ESP
        processes[i].save_ebp = NULL;   //      or EBP to return to
        processes[i].parent_pid = -1;   // Sentinal value for base level processes
        processes[i].pcb_ptr = NULL;    // Pointer to this process' PCB for convenience
    }
}

void switch_terminal(int old_term, int new_term, uint32_t esp, uint32_t ebp) {
    processes[terminal_processes[old_term]].save_esp = esp;
    processes[terminal_processes[old_term]].save_ebp = ebp;

    current_terminal = new_term;

    update_page_directory(_128MB, _8MB + terminal_processes[current_terminal] * _4MB, USER_PDE_4MB_BASE_VALUE);
    tss.esp0 = _8MB - 4 - _8KB*(terminal_processes[current_terminal]);
}

void schedule() {
    int x;
    for (x = 0; x < MAX_PROCESSES; x++) {
        printf("%d ", pid_array[x]);
    }
    printf("\n");

    uint32_t esp, ebp;
    save_esp_ebp(esp, ebp);

    // launch shell on any inactive terminals
    int i;
    for (i = 0; i < TERMINALS_COUNT; i++) {
        if (terminal_processes[i] == -1) {
            printf("Launching shell on terminal %d\n", i);
            switch_terminal(current_terminal, i, esp, ebp);
            // current_terminal = i;
            execute((uint8_t*)"shell");
            return;
        }
    }

    int next_terminal = (current_terminal + 1) % TERMINALS_COUNT;
    switch_terminal(current_terminal, next_terminal, esp, ebp);
    printf("\nSwitching to terminal %d\n>> ", current_terminal);
    esp = processes[terminal_processes[current_terminal]].save_esp;
    ebp = processes[terminal_processes[current_terminal]].save_ebp;

    restore_esp_ebp(esp, ebp);
    return;
}
