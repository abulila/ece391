#include "lib.h"
#include "pit.h"
#include "i8259.h"
#include "scheduling.h"

int counter = 0;

/*
 * PIT Initialization
 *
 * Description: ??                        // TODO: fix description
 *
 * Inputs: None
 * Outputs: None
 * Side Effects: Initializes PIT
 */
void pit_init() {
    printf("Starting PIT initialization\n");
 
    uint32_t relative = 1193180 / (14);
 
    outb(PIT_MODE_3, PIT_COMMAND_PORT);
    outb((uint8_t) (relative & 0xFF), PIT_CHANNEL_0);
    outb((uint8_t) ((relative >> 8) & 0xFF), PIT_CHANNEL_0);
 
    enable_irq(0);
 
    printf("PIT initialization finished\n");
}

/*
 * PIT Interrupt Service Routine
 *
 * Description:
 *      Function to run when PIT triggers and interrupt.
 *      Currently just prints to indicate the routine is
 *      running, but will eventually drive scheduling.
 * Inputs: None
 * Outputs: None
 * Side Effects:
 *      Prints message to display and signals EOI to PIC
 */
void pit_interrupt() {
    cli();

    send_eoi(0);
    counter++;

    if (counter % 50 == 0) {
        printf("Hello, the PIT fired! %d\n", counter);
        schedule();
    }

    sti();
}
