#include "filesystem.h"
#include "lib.h"
#include "pcb.h"
#include "types.h"

/* For the time being only read only file system with one directory. */

/* filesystem_init()
 *
 * Description:
 *      Initialize the global filesystem structures, such as the boot
 *      block and starting inode and datablock addresses.
 * Inputs: filesys_addr_input - address of filesystem as loaded by GRUB
 * Outputs: None
 * Side Effects: Initializes global filesystem structs
 */
void filesystem_init(uint32_t filesys_addr_input) {
    fs_start_address    = filesys_addr_input;
    boot_block          = (fs_info_struct*)(filesys_addr_input);
    inodes_address      = (inode_t*)(filesys_addr_input + BLOCK_SIZE);
    // increment past the boot block (1) and inode blocks (num_inodes)
    datablocks_address  = (data_block_t*)(filesys_addr_input + BLOCK_SIZE * (1 + boot_block->num_inodes));
}


/* read_dentry_by_name()
 *
 * Description:
 *      Search for a dentry object in the File System Boot Block by name
 *      and copy the data contained in this dentry to the dentry object
 *      pointed by `dentry`.  Returns 0 if a matching dentry is found
 *      and -1 if no such dentry exists.
 * Inputs: fname - string filename to search for, up to 32 bytes
 * Outputs: dentry - struct with data of matching dentry
 * Return Value: 0 on success, -1 on failure
 */
int32_t read_dentry_by_name(const uint8_t* fname, dentry_t* dentry) {
    dentry_t* dentries = boot_block->dir_entries;
    int i;

    // validate the requested file name
    if (fname[0] == 0) {
        return -1;
    }

    // iterate through each dentry in the boot block, comparing each
    // dentry name to the requested file name.  If they match (up to
    // 32 bytes), copy the data and return success.
    for(i = 0; i < MAX_DENTRY; i++) {
        if (!strncmp((int8_t*) dentries[i].filename, (int8_t*) fname, FILENAME_SIZE)) {
            memcpy(dentry, &dentries[i], sizeof(dentry_t));
            return 0; // return success on copy
        }
    }

    // no matching dentry was found, return failure
    return -1;
}

/* read_dentry_by_index()
 *
 * Description:
 *      Get a dentry object in the File System Boot Block by index and
 *      copy the data contained in this dentry to the dentry object
 *      pointed by `dentry`.  Returns 0 if index is valid and -1 otherwise.
 * Inputs: index - index to retrieve in the boot block dentry array
 * Outputs: dentry - struct with data of matching dentry
 * Return Value: 0 on success, -1 on failure
 */
int32_t read_dentry_by_index(const uint32_t index, dentry_t* dentry){
    dentry_t* dentries = boot_block->dir_entries;

    // validate the requested dentry index
    if (index > boot_block->num_dir_entries) {
        return -1;
    }

    // copy the data from the boot block and return success
    memcpy(dentry, &dentries[index], sizeof(dentry_t));
    return 0;
}

/*
 * read_data
 *
 * Description: Read data from file
 *              For a given file specified by inode number,
 *              reads length bytes to buf starting at offset bytes
 *              or until end of file.
 *              File is assumed open, that is, file_open()
 *              must have been called on this file.
 * Inputs:
 *      inode - inode number of the file from which to read data
 *      offset - number of bytes after the start of file to begin reading
 *      buf - buf to copy data from the file
 *      length - maximum number of bytes to write to buf
 * Outputs: number of bytes written to buf
 * Side Effects: Changes contents of the buffer
 */
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length){
    // validate inode number
    if (inode >= boot_block->num_inodes) {
        return -1;
    }

    // get a pointer to the requested inode
    inode_t* inode_ptr = inodes_address + inode;

    // get the length of the file from inode
    uint32_t max_length = inode_ptr->size;

    // if trying to read past end of file, return 0
    if (offset > max_length) {
        return 0;
    }

    // get a pointer to the array of data block indices stored in the inode
    uint32_t* data_block_array = inode_ptr->data_blocks;

    // calculate number of bytes to read, up to the length of the buffer
    uint32_t bytes_to_read = max_length - offset;
    if (bytes_to_read > length) {
        bytes_to_read = length;
    }
    // calculate which data block we should read first
    // and where in that block to start
    uint32_t block_index = offset / BLOCK_SIZE;     // index into inode->data_blocks[]
    uint32_t block_offset = offset % BLOCK_SIZE;    // offset into datablock_n

    int32_t data_copied = 0;

    while (data_copied < bytes_to_read) {
        // compute bytes to write from current block
        uint32_t bytes_to_write = BLOCK_SIZE - block_offset;

        // if last block is only a partial read, adjust bytes to write
        int32_t bytes_remaining = bytes_to_read - data_copied;
        if (bytes_to_write > bytes_remaining) {
            bytes_to_write = bytes_remaining;
        }

        // find the index of the next data block
        int32_t data_block_index = data_block_array[block_index];

        // verify that data block index is less than total number of data blocks
        if (data_block_index >= boot_block->data_blocks) {
            return -1;
        }

        // calculate address of actual datablock
        data_block_t* data_block_ptr = datablocks_address + data_block_index;

        // copy bytes from data block to buffer
        memcpy(&buf[data_copied], &data_block_ptr->data[block_offset], bytes_to_write);

        data_copied += bytes_to_write;

        block_offset = 0;   // reset the offset
        block_index++;      // move to next data block in inode
    }

    return data_copied;
}


/*
 * file_read
 *
 * Description: Read `count` bytes of data from file into buf
 *              For a given file specified by inode number,
 *              reads length bytes to buf starting at offset bytes
 *              or until end of file.
 *              File is assumed open, that is, file_open()
 *              must have been called on this file.
 * Inputs:
 *      fd - File descriptor for file from which to read
 *      buf - buf into which to read the data
 *      nbytes - maximum number of bytes to write to buf
 * Outputs: number of bytes written to buf
 * Side Effects: Changes contents of the buffer, updates file seek position
 */
int32_t file_read (int32_t fd, void* buf, int32_t nbytes) {
    // get the PCB and file descriptor object for fd
    pcb_t* pcb = get_pcb_ptr();
    file_desc_t* file_desc = &pcb->fd_array[fd];

    // read from the file
    int32_t bytes_read;
    bytes_read = read_data(file_desc->inode, file_desc->seek, (uint8_t*) buf, nbytes);

    // update the file's seek position
    file_desc->seek += bytes_read;
    return bytes_read;
}

/*
 * file_write
 *
 * Description: Fail to write to file
 *              Our read-only file system does not allow write.
 * Inputs:
 *      fd - File descriptor for file to which to write
 *      buf - buf from which to write the data
 *      nbytes - maximum number of bytes to write to file
 * Outputs: -1 failure for writing to a read-only file system
 * Side Effects: Developer realizes write is not supported.
 */
int32_t file_write (int32_t fd, const void* buf, int32_t nbytes) {
    return -1;
}

/*
 * file_open
 *
 * Description: "Open" a file and return a file descriptor.
 *              The actual opening is taken care of by sys_open,
 *              so just return success here.
 * Inputs: filename (ignored)
 * Outputs: Returns 0
 */
int32_t file_open (const uint8_t* filename) {
    return 0;
}

/*
 * file_close
 *
 * Description: Closes a file
 *              Marks a file as closed. Currently no operations
 *              need to be explicitly performed.
 * Inputs:
 *      fd - File descriptor for file to close
 * Outputs: 0
 * Side Effects: none
 */
int32_t file_close (int32_t fd) {
    return 0;
}


/*
 * dir_read
 *
 * Description: Read `count` bytes of data from directory into buf
 *              For a given directory specified by inode number,
 *              reads files, including `.`, filename by filename
 *              into buf. Consecutive reads will write consecutive
 *              filenames into buf. If no more files available,
 *              return 0.
 *              Directory is assumed open, that is, dir_open()
 *              must have been called on this file.
 * Inputs:
 *      fd - File descriptor for directory from which to read
 *      buf - buf into which to read the data
 *      nbytes - maximum number of bytes to write to buf
 * Outputs: number of bytes written to buf
 * Side Effects: Changes contents of the buffer
 */
int32_t dir_read (int32_t fd, void* buf, int32_t nbytes) {
    // get the PCB and file descriptor object for fd
    pcb_t* pcb = get_pcb_ptr();
    file_desc_t* file_desc = &pcb->fd_array[fd];

    // if there are no more files to read, return 0
    if (file_desc->seek >= boot_block->num_dir_entries) {
        return 0;
    }

    // read next file name from directory
    int8_t* curr_filename;
    curr_filename = (int8_t*) boot_block->dir_entries[file_desc->seek].filename;
    file_desc->seek++;

    // copy filename to buffer, stopping after the smaller
    // of nbytes and FILENAME_SIZE (filename may not be null terminated)
    int32_t bytes_to_copy = (nbytes < FILENAME_SIZE) ? nbytes : FILENAME_SIZE;
    strncpy((int8_t*) buf, curr_filename, bytes_to_copy);

    // calculate length of filename
    uint32_t len = strlen(curr_filename);
    if (len > bytes_to_copy) {
        len = bytes_to_copy;
    }

    return len;
}

/*
 * file_write
 *
 * Description: Fail to write to directory
 *              Our read-only file system does not allow write.
 * Inputs:
 *      fd - File descriptor for directory to which to write
 *      buf - buf from which to write the data
 *      nbytes - maximum number of bytes to write to file
 * Outputs: -1 failure for writing to a read-only file system
 * Side Effects: Developer realizes write is not supported.
 */
int32_t dir_write (int32_t fd, const void* buf, int32_t nbytes) {
    return -1;
}

/*
 * dir_open
 *
 * Description: Open a directory for usage
 *              Initializes temporary structures for directory
 *              by locating and caching corresponding directory name.
 *              Reset directory file iterator to beginning of directory.
 * Inputs:
 *      fd - File descriptor for directory to open
 * Outputs: 0 for success
 * Side Effects: initializes directory metadata for specified directory
 */
int32_t dir_open (const uint8_t* filename) {
    return 0;
}

/*
 * dir_close
 *
 * Description: Closes a directory file
 *              Marks a directory as closed.
 *              Unsets the cached directory name
 *              of the current directory.
 * Inputs:
 *      fd - File descriptor for directory to close
 * Outputs: 0
 * Side Effects: Unsets cached directory name
 */
int32_t dir_close (int32_t fd) {
    return 0;
}
