#include "lib.h"
#include "x86_desc.h"

#include "idt.h"
#include "interrupts.h"
#include "pcb.h"
#include "pit.h"
#include "syscalls.h"

/* Local helper functions */
static void general_exception();    // generic handlers
static void general_interrupt();
static void setup_exceptions();     // helpers to set IDT entries
static void setup_interrupts();

/*
 * Exception Handler Factory Macro
 *
 * Description:
 *      Generic macro for defining exception handlers.
 *      Each handler will print the provided message and
 *      set a flag in the PCB indicating an exception occured
 *      before halting the program.
 * Inputs:
 *      name - function name of exception handler
 *      desc - string to print when exception is raised
 * Outputs:
 *      Prints message to the screen and kills program
 */
#define EXCEPTION_HANDLER(name, desc)   \
void name() {                           \
    printf("Exception: %s\n", #desc);   \
    pcb_t* pcb = get_pcb_ptr();         \
    pcb->program_return_status = -1;    \
    while(1);                           \
    halt(0);                            \
}

/*
 * Interrupt Handler Factory Macro
 *
 * Description:
 *      Generic macro for defining interrupt handlers.
 *      Each handler will print the provided message and
 *      return control to the shell.
 * Inputs:
 *      name - function name of exception handler
 *      desc - string to print when interrupt is triggered
 * Outputs:
 *      Prints message to the screen and returns to program
 */
#define INTERRUPT_HANDLER(name, desc)   \
void name() {                           \
    cli();                              \
    printf("Interrupt: %s\n", #desc);   \
    sti();                              \
}

// Define exception handlers for IDT entries 0-19
// Exception names and descriptions from Intel IA32
// ISA Manual, vol 3, section 5.2.
EXCEPTION_HANDLER(except_DE, "Divide Error");                       // Vector 0
EXCEPTION_HANDLER(except_DB, "RESERVED");                           // Vector 1
EXCEPTION_HANDLER(except_NMI, "Non-Maskable Interrupt");            // Vector 2
EXCEPTION_HANDLER(except_BP, "Breakpoint");                         // Vector 3
EXCEPTION_HANDLER(except_OF, "Overflow");                           // Vector 4
EXCEPTION_HANDLER(except_BR, "BOUND Range Exceeded");               // Vector 5
EXCEPTION_HANDLER(except_UD, "Invalid Opcode");                     // Vector 6
EXCEPTION_HANDLER(except_NM, "Device Not Available");               // Vector 7
EXCEPTION_HANDLER(except_DF, "Double Fault");                       // Vector 8
EXCEPTION_HANDLER(except_CS, "Coprocessor Segment Overrun");        // Vector 9
EXCEPTION_HANDLER(except_TS, "Invalid TSS");                        // Vector 10
EXCEPTION_HANDLER(except_NP, "Segment Not Present");                // Vector 11
EXCEPTION_HANDLER(except_SS, "Stack-Segment Fault");                // Vector 12
EXCEPTION_HANDLER(except_GP, "General Protection");                 // Vector 13
// EXCEPTION_HANDLER(except_PF, "Page Fault");                         // Vector 14
EXCEPTION_HANDLER(except_MF, "x87 FPU Floating-Point Error");       // Vector 16
EXCEPTION_HANDLER(except_AC, "Alignment Check");                    // Vector 17
EXCEPTION_HANDLER(except_MC, "Machine Check");                      // Vector 18
EXCEPTION_HANDLER(except_XF, "SIMD Floating-Point Exception");      // Vector 19

// Define default interrupt handlers for PIC IRQ0-IRQ15
INTERRUPT_HANDLER(irq0, "IRQ 0");
INTERRUPT_HANDLER(irq1, "IRQ 1");
INTERRUPT_HANDLER(irq2, "IRQ 2");
INTERRUPT_HANDLER(irq3, "IRQ 3");
INTERRUPT_HANDLER(irq4, "IRQ 4");
INTERRUPT_HANDLER(irq5, "IRQ 5");
INTERRUPT_HANDLER(irq6, "IRQ 6");
INTERRUPT_HANDLER(irq7, "IRQ 7");
INTERRUPT_HANDLER(irq8, "IRQ 8");
INTERRUPT_HANDLER(irq9, "IRQ 9");
INTERRUPT_HANDLER(irq10, "IRQ 10");
INTERRUPT_HANDLER(irq11, "IRQ 11");
INTERRUPT_HANDLER(irq12, "IRQ 12");
INTERRUPT_HANDLER(irq13, "IRQ 13");
INTERRUPT_HANDLER(irq14, "IRQ 14");
INTERRUPT_HANDLER(irq15, "IRQ 15");

void except_PF() {
    register int error_code, eip;
    asm volatile(
        "movl 4(%%esp), %0  ;"
        "movl %%cr2, %1     ;"
        : "=r" (error_code), "=r" (eip)
    );

    printf("Exception: Page Fault\n");
    printf("         EIP: %#x\n", eip);
    printf("  Error Code: %#x\n", error_code);

    pcb_t* pcb = get_pcb_ptr();
    pcb->program_return_status = -1;
    while(1);
    halt(0);
}

// General handler for unimplemented exceptions
// Does not return control to program after printing
void general_exception() {
    printf("Exception: undefined exception\n");
    while(1);
}

// General handler for unimplemented interrupts
// Return control to program after printing
void general_interrupt() {
    cli();
    printf("Interrupt: no handler defined\n");
    sti();
}

/*
 * IDT Initialization
 *
 * Description:
 *      Initialize the Interrupt Descriptor Table
 *      to contain valid entries which point to a
 *      generic interrupt/exception handler by default.
 *      Configure specific handler functions for
 *      system exceptions and PIC interrupts.
 * Inputs: None
 * Outputs: None
 * Side Effects: Initializes IDT entries
 */
void idt_init() {
    int vec;

    // load the IDT pointer into IDTR
    lidt(idt_desc_ptr);

    for (vec = 0; vec < NUM_VEC; vec ++) {
        /* Initialize the parameters of each descriptor.
         * (From IA32 ISA Manual, vol 3, section 5.2)
         *
         *  31               16 15  14 13 12          8 7      0
         * |----------------------------------------------------|
         * |                   |   |  D  |          0  |        |
         * |    Base 31:16     | P |  P  |  0 D 1 1 /  |  0x00  |
         * |                   |   |  L  |          1  |        |
         * |----------------------------------------------------|
         *                                  ^ ^     ^   Reserved4
         *                         Reserved0  |     Reserved3
         *                                   Size
         * Bits 12:8 indicate the gate type.  Bit 12 is always zero,
         * bit 11 indicates the size (1 = 32 bits, 0 = 16 bits), and
         * bits 10:8 differentiate between Task (101), Interrupt (110),
         * and Trap (111) gates.
         */

        // Mark interrupt vector as present
        idt[vec].present = 0x1;

        // All entries have privilege level 0 except vector
        // 80 (system calls), which has privilege level 3.
        idt[vec].dpl = (vec == SYSCALL_INT) ? 0x3 : 0x0;

        // All gates are 32 bits
        idt[vec].size = 0x1;

        // Initially configure each descriptor as a Trap Gate
        idt[vec].reserved0 = 0x0;
        idt[vec].reserved1 = 0x1;
        idt[vec].reserved2 = 0x1;
        idt[vec].reserved3 = 0x1;

        // Bits 7:0 are set to 0
        idt[vec].reserved4 = 0x0;

        // These interrupts point to the Kernel Code Segment
        idt[vec].seg_selector = KERNEL_CS;

        // Assign a general exception handler to each of the
        // Intel defined trap gates and a general interrupt
        // handler to all other gates.  The exception handler
        // will spin forever and the interrupt handler will
        // return to the parent program.
        if (vec < INTEL_DEFINED) {
            SET_IDT_ENTRY(idt[vec], general_exception);
        }
        else {
            idt[vec].reserved3 = 0x0;   // mark this entry as an interrupt
            SET_IDT_ENTRY(idt[vec], general_interrupt);
        }
    }

    setup_exceptions();
    setup_interrupts();

    // Register device interrupt handlers
    SET_IDT_ENTRY(idt[TIMER_INT],    timer_handler);
    SET_IDT_ENTRY(idt[KEYBOARD_INT], keyboard_handler);
    SET_IDT_ENTRY(idt[RTC_INT],      rtc_handler);
    SET_IDT_ENTRY(idt[SYSCALL_INT],  syscall_handler);
}

/*
 * Configure system trap routines.
 * Prints the exception raised and spins.
 *
 * These following numbers are the indices for the IDT.
 */
void setup_exceptions() {
    SET_IDT_ENTRY(idt[0], except_DE);
    SET_IDT_ENTRY(idt[1], except_DB);
    SET_IDT_ENTRY(idt[2], except_NMI);
    SET_IDT_ENTRY(idt[3], except_BP);
    SET_IDT_ENTRY(idt[4], except_OF);
    SET_IDT_ENTRY(idt[5], except_BR);
    SET_IDT_ENTRY(idt[6], except_UD);
    SET_IDT_ENTRY(idt[7], except_NM);
    SET_IDT_ENTRY(idt[8], except_DF);
    SET_IDT_ENTRY(idt[9], except_CS);
    SET_IDT_ENTRY(idt[10], except_TS);
    SET_IDT_ENTRY(idt[11], except_NP);
    SET_IDT_ENTRY(idt[12], except_SS);
    SET_IDT_ENTRY(idt[13], except_GP);
    SET_IDT_ENTRY(idt[14], except_PF);
    SET_IDT_ENTRY(idt[15], general_exception);
    SET_IDT_ENTRY(idt[16], except_MF);
    SET_IDT_ENTRY(idt[17], except_AC);
    SET_IDT_ENTRY(idt[18], except_MC);
    SET_IDT_ENTRY(idt[19], except_XF);
}

/*
 * Configure generic interrupt handlers for
 * each IRQ line. Simply print the IRQ number.
 *
 * These following numbers are the offsets for the PIC
 * vectors, starting at 0x20 (32).
 */
void setup_interrupts() {
    SET_IDT_ENTRY(idt[32], irq0);
    SET_IDT_ENTRY(idt[33], irq1);
    SET_IDT_ENTRY(idt[34], irq2);
    SET_IDT_ENTRY(idt[35], irq3);
    SET_IDT_ENTRY(idt[36], irq4);
    SET_IDT_ENTRY(idt[37], irq5);
    SET_IDT_ENTRY(idt[38], irq6);
    SET_IDT_ENTRY(idt[39], irq7);
    SET_IDT_ENTRY(idt[40], irq8);
    SET_IDT_ENTRY(idt[41], irq9);
    SET_IDT_ENTRY(idt[42], irq10);
    SET_IDT_ENTRY(idt[43], irq11);
    SET_IDT_ENTRY(idt[44], irq12);
    SET_IDT_ENTRY(idt[45], irq13);
    SET_IDT_ENTRY(idt[46], irq14);
    SET_IDT_ENTRY(idt[47], irq15);
}
