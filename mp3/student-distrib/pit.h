#ifndef _PIT_H
#define _PIT_H


#define PIT_COMMAND_PORT    0x43
#define PIT_CHANNEL_0       0x40
#define PIT_MODE_3          0x36  /* Channel 0, access low + high bytes, mode 3, 16-bit binary mode */

/* Initialize the PIT */
void pit_init();
/* PIT Interrupt Handler */
void pit_interrupt();

#endif
