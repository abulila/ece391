/* types.h - Defines to use the familiar explicitly-sized types in this
 * OS (uint32_t, int8_t, etc.).  This is necessary because we don't want
 * to include <stdint.h> when building this OS
 * vim:ts=4 noexpandtab
 */

#ifndef _TYPES_H
#define _TYPES_H

#define NULL    0
#define _4KB    0x00001000
#define _8KB    0x00002000
#define _4MB    0x00400000
#define _8MB    0x00800000
#define _128MB  0x08000000
#define _132MB  0x08400000
#define _136MB  0x08800000

#define ASCII_SP    ' '
#define ASCII_DEL   '\b'
#define ASCII_LF    '\n'
#define ASCII_CR    '\r'
#define ASCII_NUL   '\0'

#ifndef ASM

/* Types defined here just like in <stdint.h> */
typedef int int32_t;
typedef unsigned int uint32_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef char int8_t;
typedef unsigned char uint8_t;

// Number of Default colors and an enum of each color from 0x0 to 0xF
#define NUM_COLORS 16
typedef enum {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    ORANGE,
    WHITE,
    GRAY,
    PURPLE,
    BRIGHT_GREEN,
    BRIGHT_CYAN,
    BRIGHT_RED,
    BRIGHT_MAGENTA,
    YELLOW,
    BRIGHT_WHITE
} color;

#endif /* ASM */

#endif /* _TYPES_H */
