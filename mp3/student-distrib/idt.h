#ifndef _IDT_H
#define _IDT_H

/* INT Vectors
 * source: lecture slides
 * (https://courses.engr.illinois.edu/ece391/fa2018/secure/fa18_lectures/ECE391_Lecture12(F).pdf)
 */
#define TIMER_INT       0x20
#define KEYBOARD_INT    0x21
#define RTC_INT         0x28
#define SYSCALL_INT     0x80

// number of Intel defined interrupts
#define INTEL_DEFINED   32

/* Initialize the IDT. */
void idt_init();


#endif // IDT_H
