#ifndef _INTERRUPTS_H
#define _INTERRUPTS_H

#ifndef ASM

extern void timer_handler();
extern void rtc_handler();
extern void keyboard_handler();
extern int32_t syscall_handler();

#endif
#endif // _INTERRUPTS_H
