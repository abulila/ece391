#include "lib.h"
#include "paging.h"

/*
 * Paging Init Function
 *
 * Description:
 *      Initialize and enable paging with appropriate starting page directory and table entries.
 * Inputs:
 *      none
 * Outputs:
 *      none
 * Side Effects:
 *      First two page directories set up for video memory and kernel memory respectively.
 *      Other page directories cleared.
 *      Video memory kernel page made present and enabled.
 * Source: OSdev wiki
 * https://wiki.osdev.org/Paging
 */
void paging_init(void) {
    /* clear page directory and page table memory */
    memset(page_directory, CLEAR, PAGE_DIRECTORY_SIZE*sizeof(uint32_t));
    memset(page_tables, CLEAR, PAGE_TABLE_SIZE*sizeof(uint32_t));

    /*
     * set page directory entries for video memory and kernel with
     * appropriate bookkeeping flags in the low bits
     */
    page_directory[VID_MEM_PD_INDEX] = USER_PDE_BASE_VALUE | ((uint32_t) page_tables);
    page_directory[KERNEL_MEM_PD_INDEX] = KERNEL_PDE_BASE_VALUE;

    /*
     * set the appropriate location for the page table to point
     * to the physical address of video memory, also setting the
     * appropriate flags in the low bits
     */
    page_tables[VID_MEM_PT_INDEX] = VIDEO | PAGE_TABLE_PRESENT_ENTRY;

    /* set bits in cr3, cr4, and cr0 to enable paging */
    asm volatile("                   \n\
        pushl %%eax                  \n\
        movl %0, %%eax               \n\
        movl %%eax, %%cr3            \n\
        movl %%cr4, %%eax            \n\
        orl %1, %%eax                \n\
        movl %%eax, %%cr4            \n\
        movl %%cr0, %%eax            \n\
        orl %2, %%eax                \n\
        movl %%eax, %%cr0            \n\
        popl %%eax                   \n\
        "
        : /* no outputs */
        : "b" (page_directory) /* Address of page directory into cr3 (pdbr) */
        , "c" (ASM_MASK_CR4)
        , "d" (ASM_MASK_CR0)
        : "eax" /* eax clobbered bc it's clobberin' time */
    );
}

/*
 * Update page directory mapping from virt_address to point
 * to phys_address.
 */
void update_page_directory(uint32_t virt_address, uint32_t phys_address, uint16_t flags) {
    page_directory[virt_address >> PDE_IDX_SHIFT] = phys_address | flags;
    flush_tlb();
}

/*
 * Update userspace page table to map virt_address to point to system video memory
 */
void remap_video(uint32_t virt_address) {
    // add entry to PD pointing to video page table
    page_directory[virt_address >> PDE_IDX_SHIFT] = (uint32_t)page_tables | PAGE_TABLE_PRESENT_ENTRY;
    // point first entry to physical video memory
    page_tables[VID_MEM_PT_INDEX] = VIDEO | PAGE_TABLE_PRESENT_ENTRY;
    flush_tlb();
}

/*
 * Flush the TLB by writing to CR3.  We don't actually
 * want to change the value, so write CR3 back to CR3.
 */
void flush_tlb(void){
    asm volatile("                   \n\
        movl %%cr3, %%eax            \n\
        movl %%eax, %%cr3            \n\
        "
        : /* no outputs */
        : /* no inputs  */
        : "eax" /* eax clobbered bc it's clobberin' time */
    );
}
