/* paging.h - Defines used in interactions with the paging
 * initialization.
 * vim:ts=4 noexpandtab
 */

#ifndef _PAGING_H
#define _PAGING_H

#include "types.h"

#define PAGE_DIRECTORY_SIZE         1024        /* Number of entries in page directory          */
#define PAGE_TABLE_SIZE             1024        /* Number of entries in page table              */
#define PAGING_BYTE_ALIGNMENT       4096

#define USER_PDE_BASE_VALUE         0x07        /* USER / READ+WRITE / PRESENT                  */
#define USER_PDE_4MB_BASE_VALUE     0x87        /* 4MB / USER / READ+WRITE / PRESENT            */
#define KERNEL_PDE_BASE_VALUE       0x400083    /* ADDR+4kb_page+Supervisor+readwrite+present   */
#define PAGE_TABLE_PRESENT_ENTRY    0x7         /* USER/READ+WRITE/PRESENT                      */

#define PAGE_ADDRESS_ONLY_MASK      0xFFFFF000

#define VID_MEM_PD_INDEX            0           /* Video memory in first page directory entry   */
#define KERNEL_MEM_PD_INDEX         1           /* Kernel memory in second page directory entry */

#define CLEAR                       0
#define VIDEO                       0xB8000     /* Address of video memory page                 */

#define BITS_4KB_ALIGN              12
#define VID_MEM_PT_INDEX            (VIDEO >> BITS_4KB_ALIGN)

#define ASM_MASK_CR4                0x10        /* Mask to turn on PSE in CR4                   */
#define ASM_MASK_CR0                0x80000000  /* the mask to turn PG on in control register 0 */

#define PDE_IDX_SHIFT               22          /* Page directory index in virt. address        */

void paging_init(void);
void update_page_directory(uint32_t virt_address, uint32_t phys_address, uint16_t flags);
void remap_video(uint32_t virt_address);
void flush_tlb(void);

uint32_t page_directory[PAGE_DIRECTORY_SIZE] __attribute__((aligned (PAGING_BYTE_ALIGNMENT)));
uint32_t page_tables[PAGE_TABLE_SIZE] __attribute__((aligned (PAGING_BYTE_ALIGNMENT)));
uint32_t video_page_table[PAGE_TABLE_SIZE] __attribute__((aligned (PAGING_BYTE_ALIGNMENT)));


#endif /* _PAGING_H */
