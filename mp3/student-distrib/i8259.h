/* i8259.h - Defines used in interactions with the 8259 interrupt
 * controller
 * vim:ts=4 noexpandtab
 */

#ifndef _I8259_H
#define _I8259_H

#include "types.h"

/* Ports that each PIC sits on */
/* From OSdev wiki: https://wiki.osdev.org/PIC */
#define PIC1            0x20
#define PIC2            0xA0
#define MASTER_CMD      PIC1
#define MASTER_DATA     (PIC1 + 1)
#define SLAVE_CMD       PIC2
#define SLAVE_DATA      (PIC2 + 1)

/* Initialization control words to init each PIC.
 * See the Intel manuals for details on the meaning
 * of each word */
#define ICW1                0x11
#define OFFSET_MASTER       0x20
#define OFFSET_SLAVE        0x28
#define ICW3_MASTER         0x04
#define ICW3_SLAVE          0x02
#define ICW4_8086           0x01

/* End-of-interrupt byte.  This gets OR'd with
 * the interrupt number and sent out to the PIC
 * to declare the interrupt finished */
#define EOI                 0x60

// To check if we are using master or slave PIC
#define PIC_IDENTIFIER 8
#define MASK_ALL 0xFF

/* Externally-visible functions */

/* Initialize both PICs */
void i8259_init(void);
/* Enable (unmask) the specified IRQ */
void enable_irq(uint32_t irq_num);
/* Disable (mask) the specified IRQ */
void disable_irq(uint32_t irq_num);
/* Send end-of-interrupt signal for the specified IRQ */
void send_eoi(uint32_t irq_num);

#endif /* _I8259_H */
