#ifndef _SCHEDULING_H
#define _SCHEDULING_H

#include "types.h"
#include "syscalls.h"
#include "filesystem.h"
#include "pcb.h"
#include "paging.h"

#define TERMINALS_COUNT 3

#define save_esp_ebp(esp, ebp)      \
do {                                \
    asm volatile(                   \
        "movl %%esp, %%eax;"        \
        "movl %%ebp, %%ebx;"        \
        : "=a" (esp), "=b" (ebp)    \
    );                              \
} while(0);

#define restore_esp_ebp(esp, ebp)   \
do {                                \
    asm volatile(                   \
        "movl %%eax, %%esp;"        \
        "movl %%ebx, %%ebp;"        \
        : /* no outputs */          \
        : "a" (esp), "b" (ebp)      \
    );                              \
} while(0);


typedef struct {
    uint32_t save_esp;
    uint32_t save_ebp;
    int32_t parent_pid;
    pcb_t * pcb_ptr;
    uint8_t home_terminal;
} process_info_t;

uint8_t current_terminal;

int8_t terminal_processes[TERMINALS_COUNT]; //holds pid of program running at highest level

process_info_t processes[MAX_PROCESSES];


void schedule_init();
void schedule();


#endif
