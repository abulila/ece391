#include "types.h"

#include "filesystem.h"
#include "paging.h"
#include "pcb.h"
#include "syscalls.h"
#include "scheduling.h"

uint32_t current_program_count = 0;
uint8_t exec_args[128]; /* Max length of arguments. TODO match actual max length and put args here */
uint8_t elf_identifier[] = {0x7f, 0x45, 0x4c, 0x46}; /* literal magic numbers from the executable file specification */

uint8_t pid_array[MAX_PROCESSES] = {0};
uint8_t active_process = 0;

/*
 * Syscall Halt
 *
 * Description:
 *      Halt current process and return to execution completion.
 * Inputs:
 *      status - Exit status of process.
 * Outputs:
 *      None (jmps to execute's end)
 * Side effects:
 *      Zero extends program status for sending to execution resolution.
 *      Marks process as completed.
 *      Closes file descriptor array.
 *      Updates page directory to reflect new process memory mapping.
 *      Mark parent process as the active process.
 *      Restores parent process esp, ebp and jumps to execute end.
 */
int32_t halt(uint8_t status) {
    // printf("HALT REACHED\n");
    if (status != 0) printf("%d\n", status);

    pcb_t* curr_pcb = processes[terminal_processes[current_terminal]].pcb_ptr;

    // if program exited with an exception, return 256 from the spec
    uint32_t return_status = (uint32_t)status;
    if (curr_pcb->program_return_status == -1) {
        return_status = 256;
    }

    // free up markers
    pid_array[curr_pcb->pid] = PROCESS_INACTIVE;
    if (curr_pcb->parent_pid == -1) {
        terminal_processes[current_terminal] = -1;
    }
    else {
        // restore memory mapping
        terminal_processes[current_terminal] = curr_pcb->parent_pid;
        update_page_directory(_128MB, _8MB + curr_pcb->parent_pid * _4MB, PDE_4MB_USR_RW_P_FLAGS);
    }

    // close all file descriptors
    int i;
    for (i = 0; i < MAX_FD; i++) {
        if (curr_pcb->fd_array[i].flags == IN_USE) {
            close(i);
        }
        curr_pcb->fd_array[i].fops_jump_table = &no_ops;
        curr_pcb->fd_array[i].flags = NOT_IN_USE;
    }

    // restore esp0 in tss
    tss.esp0 = curr_pcb->save_esp0;

    // restore ESP/EBP, set return status and jump back to execute
    restore_esp_ebp(curr_pcb->save_ksp, curr_pcb->save_kbp);
    asm volatile(
        "movl %0, %%eax         ;"
        "jmp IRET_RETURN_LABEL  ;"
        : /* no outputs */
        : "r" (return_status)
        : "eax"
    );

    // this should never run because of jmp above
    return 0;
}

/*
 * Syscall Execute
 *
 * Description:
 *      Executes a given command by saving the current process info and
 *       setting up the user process, then executing it.
 * Inputs:
 *      command - String reflecting the command input.
 * Outputs:
 *      Returns the child process result (through EAX)
 */
int32_t execute(const uint8_t* command) {
    uint8_t filename[MAX_FILENAME_LENGTH];
    uint32_t cmd_start, cmd_end, cmd_args;
    uint8_t arg_buf[TERM_KEY_BUF_SIZE];
    uint8_t buffer[4];              /* Buffer to check ELF validity (first 4 bytes) */
    uint32_t entry_point;
    int32_t process_number;

    // null check command input
    if (command == NULL) {
        return -1;
    }

    //-----------------------------------------
    //  Parse Program Name
    //-----------------------------------------

    // trim any leading spaces from the command
    cmd_start = 0;
    while (command[cmd_start] == ' '){
        cmd_start++;
    }

    // find the end of the string/line/word, whichever comes first
    cmd_end = cmd_start;
    while (command[cmd_end] != ' ' &&
           command[cmd_end] != ASCII_NUL &&
           command[cmd_end] != ASCII_LF) {
        cmd_end++;
    }

    // find the beginning of the arguments
    cmd_args = cmd_end + 1;
    while (command[cmd_args] == ' '){
        cmd_args++;
    }

    // Copy first word of command into filename
    strncpy((int8_t*)filename, (int8_t*)(command + cmd_start), cmd_end - cmd_start);
    filename[cmd_end - cmd_start] = ASCII_NUL;

    // Copy remaining command into arg buffer
    strncpy((int8_t*)arg_buf, (int8_t*)(command+cmd_args), TERM_KEY_BUF_SIZE);
    arg_buf[TERM_KEY_BUF_SIZE-1] = ASCII_NUL;

    //-----------------------------------------
    //  Validate Executable
    //-----------------------------------------
    dentry_t dir_entry;

    // make sure file exists
    if (read_dentry_by_name(filename, &dir_entry) != 0) {
        return -1;
    }

    // make sure file is an executable
    read_data(dir_entry.inode_number, 0, buffer, sizeof(buffer));
    if (strncmp((int8_t*)buffer, (int8_t*)elf_identifier, sizeof(buffer)) != 0) {
        return -1;
    }

    // get the program entry point (bytes 24 - 27)
    read_data(dir_entry.inode_number, 24, buffer, sizeof(buffer));
    entry_point = *((uint32_t*)buffer);

    pcb_t* parent_pcb = get_pcb_ptr();  // get PCB based on %ESP
    // DEBUG:
    parent_pcb = processes[terminal_processes[current_terminal]].pcb_ptr;

    // get a process number
    if ((process_number = get_next_process_number()) == -1) {
        return -1;
    }
    terminal_processes[current_terminal] = process_number;

    //-----------------------------------------
    //  Modify Paging / Load File
    //-----------------------------------------

    update_page_directory(_128MB, _8MB + process_number * _4MB, USER_PDE_4MB_BASE_VALUE);

    // copy entire program into virtual memory
    // program is guaranteed to fit in a single block
    read_data(dir_entry.inode_number, 0, (uint8_t*)PROGRAM_VIRT_ADDR, _4MB);

    //-----------------------------------------
    //  Configure PCB
    //-----------------------------------------

    pcb_t* new_pcb = get_pcb_for_process(process_number);

    if (/*parent_pcb >= (pcb_t*)(_8MB - _8KB)*/parent_pcb == NULL) {
        // This execute was called from the kernel,
        // set sentinal values for parent info
        new_pcb->parent_pid = -1;
        processes[process_number].parent_pid = -1;
    }
    else {
        new_pcb->parent_pid = parent_pcb->pid;
        processes[process_number].parent_pid = parent_pcb->pid;
    }

    new_pcb->pid = process_number;
    processes[process_number].home_terminal = current_terminal;
    processes[process_number].pcb_ptr = new_pcb;

    // Save current %ESP and %EBP values for later
    uint32_t esp, ebp;
    save_esp_ebp(esp, ebp);

    new_pcb->save_kbp = ebp;
    new_pcb->save_ksp = esp;
    processes[process_number].save_ebp = ebp;
    processes[process_number].save_esp = esp;

    // copy the arguments into the PCB
    strncpy((int8_t*)new_pcb->arg_buf, (int8_t*)arg_buf, TERM_KEY_BUF_SIZE);
    new_pcb->arg_buf[sizeof(new_pcb->arg_buf) - 1] = ASCII_NUL;

    // initialize file descriptors (including stdin/stdout)
    pcb_init_fd(new_pcb);

    //-----------------------------------------
    //  Context Switch
    //-----------------------------------------

    new_pcb->save_esp0 = tss.esp0;
    tss.esp0 = _8MB - 4 - _8KB*(process_number+1);

    /*
     * Make a fake iret context and "return" to user space
     * Credit to https://wiki.osdev.org/Getting_to_Ring_3
     */
    asm volatile(
        "mov %2, %%ds       ;"  /* Update DS                            */
        "pushl %2           ;"  /* Push new SS                          */
        "pushl %1           ;"  /* Push new ESP (bottom of 4MB page)    */
        "pushfl             ;"  /* Push flags                           */
        "pushl %3           ;"  /* Push new CS                          */
        "pushl %0           ;"  /* Push user-space return address       */
        "iret               ;"  /* "Return" to user-space               */
        "IRET_RETURN_LABEL: ;"  /* Return point from halt()             */
        "leave              ;"  /* Leave and return from execute(),     */
        "ret                ;"  /* EAX contains status from halt()      */
        : /* no outputs */
        : "r" (entry_point), "r" (PAGE_BOTTOM), "r" (USER_DS), "r" (USER_CS)
        : "eax"
    );

    // this should never run because of ret above
    return 0;
}

/*
 * Return the next available process number
 * or -1 if no more processes may be run.
 */
int32_t get_next_process_number() {
    int32_t i;
    for (i = 0; i < MAX_PROCESSES; i++) {
        if (pid_array[i] == PROCESS_INACTIVE) {
            pid_array[i] = PROCESS_ACTIVE;
            return i;
        }
    }
    return -1;
}

/*
 * Syscall Read
 *
 * Description:
 *      Read from a file descriptor into buffer
 * Inputs:
 *      fd - file descriptor index (0 - 7)
 *      buf - buffer to store data read
 *      nbytes - amount of data to read from fd
 * Outputs:
 *      Returns number of bytes actually written to buf
 */
int32_t read(int32_t fd, void* buf, int32_t nbytes) {
    // pcb_t* pcb = get_pcb_ptr();
    pcb_t* pcb = get_pcb_for_process(terminal_processes[current_terminal]);

    /* Argument validation */
    if (fd < 0 || fd >= MAX_FD) {
        // printf("Invalid file descriptor %d\n", fd);
        return -1;
    }
    if (buf == NULL) {
        // printf("Buffer may not be NULL\n");
        return -1;
    }
    if (nbytes < 0) {
        // printf("nbytes must be >= 0\n");
        return -1;
    }

    /* Check if file is open */
    if (pcb->fd_array[fd].flags == NOT_IN_USE) {
        // printf("File descriptor %d is not open\n", fd);
        return -1;
    }

    /* Call the file-specific read function */
    return pcb->fd_array[fd].fops_jump_table->read(fd, buf, nbytes);
}

/*
 * Syscall Write
 *
 * Description:
 *      Write data from a buffer to a file descriptor
 * Inputs:
 *      fd - file descriptor index (0 - 7)
 *      buf - buffer to read data from
 *      nbytes - amount of data to write (size of buffer)
 * Outputs:
 *      Returns number of bytes actually written to fd
 */
int32_t write(int32_t fd, const void* buf, int32_t nbytes) {
    // pcb_t* pcb = get_pcb_ptr();
    pcb_t* pcb = get_pcb_for_process(terminal_processes[current_terminal]);


    /* Argument validation */
    if (fd < 0 || fd >= MAX_FD) {
        // printf("Invalid file descriptor %d\n", fd);
        return -1;
    }
    if (buf == NULL) {
        // printf("Buffer may not be NULL\n");
        return -1;
    }
    if (nbytes < 0) {
        // printf("nbytes must be >= 0\n");
        return -1;
    }

    /* Check if file is open */
    if (pcb->fd_array[fd].flags == NOT_IN_USE) {
        // printf("File descriptor %d is not open\n", fd);
        return -1;
    }

    /* Call the file-specific write function */
    return pcb->fd_array[fd].fops_jump_table->write(fd, buf, nbytes);
}

/*
 * Syscall Open
 *
 * Description:
 *      Create a new file descriptor for specified file
 * Inputs:
 *      filename - file to open
 * Outputs:
 *      Returns file descriptor or -1 on failure
 */
int32_t open(const uint8_t* filename) {
    dentry_t file_dentry;
    int32_t fd, i;
    // pcb_t* pcb = get_pcb_ptr();
    pcb_t* pcb = get_pcb_for_process(terminal_processes[current_terminal]);

    /* Search for the specified file by name and return -1 on error */
    if (-1 == read_dentry_by_name(filename, &file_dentry)) {
        // printf("File not found: %s\n", filename);
        return -1;
    }

    /* Find an unused file descriptor and allocate it for this file */
    for (i = 0; i < MAX_FD; i++) {
        if (pcb->fd_array[i].flags == NOT_IN_USE) {
            fd = i;
            pcb->fd_array[fd].flags = IN_USE;
            pcb->fd_array[fd].seek = SEEK_SET;
            break;
        }
    }

    /* If no free file descriptor was found, return -1 */
    if (i == MAX_FD) {
        // printf("No free file descriptors\n");
        return -1;
    }

    /* Finish setting up the file descriptor based on file type */
    switch (file_dentry.filetype) {
        case 0:     // RTC
            pcb->fd_array[fd].fops_jump_table = &rtc_ops;
            pcb->fd_array[fd].inode = -1;
            break;
        case 1:     // Dir
            pcb->fd_array[fd].fops_jump_table = &dir_ops;
            pcb->fd_array[fd].inode = -1;
            break;
        case 2:     // File
            pcb->fd_array[fd].fops_jump_table = &file_ops;
            pcb->fd_array[fd].inode = file_dentry.inode_number;
    }

    /* Call the file-specific open function */
    /* Note: this essentially does nothing except in the case of RTC */
    pcb->fd_array[fd].fops_jump_table->open(filename);

    /* Return the file descriptor */
    return fd;
}

/*
 * Syscall Close
 *
 * Description:
 *      Close the specified file descriptor
 * Inputs:
 *      fd - file descriptor index (0 - 7)
 * Outputs:
 *      Returns 0 on success, -1 on failure
 */
int32_t close(int32_t fd) {
    // pcb_t* pcb = get_pcb_ptr();
    pcb_t* pcb = get_pcb_for_process(terminal_processes[current_terminal]);

    /* Argument validation */
    if (fd < 2 || fd >= MAX_FD) {
        // printf("Invalid file descriptor %d\n", fd);
        return -1;
    }

    /* File can't be closed if it isn't open */
    if (pcb->fd_array[fd].flags == NOT_IN_USE) {
        // printf("File descriptor %d is already closed\n", fd);
        return -1;
    }

    /* Mark the file as closed */
    pcb->fd_array[fd].flags = NOT_IN_USE;

    /* Call the file-specific close function */
    return pcb->fd_array[fd].fops_jump_table->close(fd);
}

/****************************** CHECKPOINT 4 ******************************/

/*
 * Syscall GetArgs
 *
 * Description:
 *      Copies arguments from command line into user space
 * Inputs:
 *      buf - User space buffer to copy arguments to.
 *      nbytes - number of bytes to copy (size of buf).
 * Outputs:
 *      Returns 0 on success.
 */
int32_t getargs(uint8_t* buf, int32_t nbytes) {

    // Check if arguments are valid
    if (buf == NULL) {
        return -1;
    }
    if (nbytes < 0) {
        return -1;
    }

    // Get pcb pointer to access arguments buffer
    pcb_t* pcb = processes[terminal_processes[current_terminal]].pcb_ptr;
    // pcb_t* pcb = get_pcb_for_process(terminal_processes[current_terminal]);

    int str_len = strlen((int8_t*) pcb->arg_buf);

    // Check if arguments exist or if buffer size is sufficient
    if(str_len == 0){
        return -1;
    }
    if (str_len >= nbytes) {
        return -1;
    }

    // if everything is valid copy arguments to user space and return success
    strncpy((int8_t*) buf, (int8_t*) pcb->arg_buf, nbytes);
    return 0;
}

/*
 * Syscall VidMap
 *
 * Description:
 *      Sets up paging to point userspace address to system video memory
 * Inputs:
 *      screen_start - pointer to userspace variable which
 *                     stores location of video memory.
 * Outputs:
 *      Returns 0 on success, -1 on failure.
 */
int32_t vidmap(uint8_t** screen_start) {
    // check for null pointer
    if (screen_start == NULL) {
        return -1;
    }

    //Checks that it is within program space for validity
    if((uint32_t)screen_start < _128MB || (uint32_t)screen_start >= _132MB) {
        return -1;
    }

    // map userspace video memory to 4KB page starting at
    // 132MB and report starting address via screen_start pointer
    remap_video(USER_VID_MEM_ADDR);
    *screen_start = (uint8_t*)USER_VID_MEM_ADDR;
    return 0;
}

/****************************** EXTRA CREDIT ******************************/

int32_t set_handler(int32_t signum, void* handler_address) {
    return -1;
}

int32_t sigreturn(void) {
    return -1;
}
