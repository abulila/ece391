/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "lib.h"

/* Interrupt masks to determine which interrupts are enabled and disabled */
uint8_t master_mask; /* IRQs 0-7  */
uint8_t slave_mask;  /* IRQs 8-15 */
/* Masks are Active Low, and 1-hot */

/*
 * PIC Init Function
 *
 * Description:
 *      Initialize the 8259 PIC
 * Inputs: None
 * Outputs: None
 * Side Effects:
 *      All interrupts masked except IRQ2 for Slave PIC
 * Source: Lecture Slides
 * (https://courses.engr.illinois.edu/ece391/fa2018/secure/fa18_lectures/ECE391_Lecture9(F).pdf)
 */
void i8259_init(void) {
    // Similar to PIC initializatoin from Lecture slides but without auto_eoi and

    // Master INIT
    outb(ICW1,          MASTER_CMD);
    outb(OFFSET_MASTER, MASTER_DATA);
    outb(ICW3_MASTER,   MASTER_DATA);
    outb(ICW4_8086,     MASTER_DATA);

    // Slave INIT
    outb(ICW1,         SLAVE_CMD);
    outb(OFFSET_SLAVE, SLAVE_DATA);
    outb(ICW3_SLAVE,   SLAVE_DATA);
    outb(ICW4_8086,    SLAVE_DATA);

    master_mask = MASK_ALL;
    slave_mask = MASK_ALL;
    outb(master_mask, MASTER_DATA); // Mask Master
    outb(slave_mask, SLAVE_DATA);   // Mask Slave

    enable_irq(2);      // Enable Slave PIC on IRQ 2
}

/*
 * IRQ Enable Function
 *
 * Description:
 *      Enable (unmask) the specified IRQ.  Bitwise AND
 *      the current mask with a bitvector of 1s with a 0
 *      at the bit position for the IRQ.
 * Inputs:
 *      irq_num - IRQ number to enable (0-15)
 * Outputs:
 *      none
 * Side Effects:
 *      Enables IRQ line on PIC
 */
void enable_irq(uint32_t irq_num) {
    uint8_t port;
    uint8_t value;

    // printf("Enable IRQ %d\n", irq_num);

    if (irq_num < PIC_IDENTIFIER){   // Master PIC
        port = MASTER_DATA;
    }
    else {                          // Slave PIC
        port = SLAVE_DATA;
        irq_num -= PIC_IDENTIFIER;
    }

    // shift a 1 into the correct position and invert
    // to get a single zero at that bit.  AND with the
    // current value to clear that bit.
    value = inb(port) & ~(1 << irq_num);
    outb(value, port);

    // update global variables
    if (port == MASTER_DATA) {
        master_mask = value;
    }
    else {
        slave_mask = value;
    }
}

/*
 * IRQ Enable Function
 *
 * Description:
 *      Disable (mask) the specified IRQ.  Bitwise OR
 *      the current mask with a bitvector of 0s with a 1
 *      at the bit position for the IRQ.
 * Inputs:
 *      irq_num - IRQ number to enable (0-15)
 * Outputs:
 *      none
 * Side Effects:
 *      Disables IRQ line on PIC
 */
void disable_irq(uint32_t irq_num) {
    uint8_t port;
    uint8_t value;

    // printf("Disable IRQ %d\n", irq_num);

    if (irq_num < PIC_IDENTIFIER){   // Master PIC
        port = MASTER_DATA;
    }
    else {                          // Slave PIC
        port = SLAVE_DATA;
        irq_num -= PIC_IDENTIFIER;
    }

    // shift a 1 into the correct position and OR it
    // with the current value to set that bit
    value = inb(port)| (1 << irq_num);
    outb(value, port);

    // update global variables
    if (port == MASTER_DATA) {
        master_mask = value;
    }
    else {
        slave_mask = value;
    }
}

/*
 * Send EOI Function
 *
 * Description:
 *      Send end-of-interrupt signal for the specified IRQ
 * Inputs:
 *      irq_num - IRQ number to enable (0-15)
 * Outputs:
 *      none
 * Side Effects:
 *      Unsets IRQ in PIC in service register
 */
void send_eoi(uint32_t irq_num) {
    uint8_t port;
    uint8_t value;

    if (irq_num < PIC_IDENTIFIER) { // Master PIC
        port = MASTER_CMD;
    }
    else {                          // Slave PIC
        port = SLAVE_CMD;
        irq_num -= PIC_IDENTIFIER;
        // Send EOI to Master IRQ-2 which is connected to Slave PIC
        outb(EOI | 2, MASTER_CMD);
    }
    value = EOI | irq_num;  // OR EOI With interrupt number
    outb(value, port);      // Send EOI
}
