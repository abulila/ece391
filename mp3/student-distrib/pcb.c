#include "pcb.h"

fops_jumptable_t stdin_ops  = { terminal_read,   file_op_invalid, terminal_open,   terminal_close  };
fops_jumptable_t stdout_ops = { file_op_invalid, terminal_write,  terminal_open,   terminal_close  };
fops_jumptable_t rtc_ops    = { rtc_read,        rtc_write,       rtc_open,        rtc_close       };
fops_jumptable_t dir_ops    = { dir_read,        dir_write,       dir_open,        dir_close       };
fops_jumptable_t file_ops   = { file_read,       file_write,      file_open,       file_close      };
fops_jumptable_t no_ops     = { file_op_invalid, file_op_invalid, file_op_invalid, file_op_invalid };

/* Initialize PCB struct */
void pcb_init_fd(pcb_t* pcb){
    /* Set all file descriptors to be initially empty */
    int i;
    for (i = 0; i < MAX_FD; i++) {
        pcb->fd_array[i].fops_jump_table = &no_ops;
        pcb->fd_array[i].inode = -1;
        pcb->fd_array[i].seek = SEEK_SET;
        pcb->fd_array[i].flags = NOT_IN_USE;
    }

    /* Set up stdin (fd 0) and stdout (fd 1) */
    pcb->fd_array[STDIN_FILENO].fops_jump_table = &stdin_ops;
    pcb->fd_array[STDIN_FILENO].flags = IN_USE;

    pcb->fd_array[STDOUT_FILENO].fops_jump_table = &stdout_ops;
    pcb->fd_array[STDOUT_FILENO].flags = IN_USE;
}

/*
 * Get the pointer to the PCB located at the
 * the top of this process' stack
 */
pcb_t* get_pcb_ptr(void) {
    pcb_t* pcb;
    asm volatile("              \n\
            andl %%esp, %%eax   \n\
            "
            : "=a"(pcb)
            : "a"(PCB_PTR_MASK)
            : "cc"
    );
    return pcb;
}

/*
 * Get pointer to PCB for a given process ID.
 * PCB is located at the top of an 8 KB chunk
 * in the kernel page, where each process has
 * it's own chunk.  The first chunk is located
 * at 8 MB - 8 KB (end of kernel page - 8 KB).
 * Offset by pid+1 to get the top of the PCB
 * segment in the kernel stack.
 */
pcb_t* get_pcb_for_process(uint32_t pid) {
    pid++;  // increment to skip first 8 KB section in kernel stack
    pid++;  // increment to account for zero indexed PID
    return (pcb_t*)(_8MB - pid * _8KB);
}

/*
 * Dummy function for unimplemented file operations
 */
int32_t file_op_invalid() {
    return -1;
}
