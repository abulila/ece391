/* keyboard.c - Functions used in keyboard interrupt handler
 * tabsize:4, soft tabs
 */
#include "keyboard.h"
#include "types.h"
#include "scheduling.h"

// Keycode Defines
#define LEFT_SHIFT_PRESS        0x2A
#define RIGHT_SHIFT_PRESS       0x36
#define CONTROL_PRESS           0x1D
#define CAPS_LOCK_PRESS         0x3A
#define SCROLL_LOCK_PRESS       0x46
#define NUM_LOCK_PRESS          0x45
#define ALT_PRESS               0x38
#define LEFT_SHIFT_RELEASE      (LEFT_SHIFT_PRESS + 0x80)
#define RIGHT_SHIFT_RELEASE     (RIGHT_SHIFT_PRESS + 0x80)
#define CONTROL_RELEASE         (CONTROL_PRESS + 0x80)
#define ALT_RELEASE             (ALT_PRESS + 0x80)

#define BACKSPACE               0x0E
#define TAB                     0x0F
#define ENTER                   0x1C
#define INSERT                  0x52
#define DELETE                  0x53

// Chars for CTRL commands
#define L_KEY   0x26
#define C_KEY   0x2E

// Special Character Handling
#define TAB_STRING      "    "
#define TAB_LENGTH      4

#define UPPERCASE 32    // 'A' + 32 = 'a'; ASCII uppercase converter

// #define KEYBOARD_COLOR 0x7

/* Private Functions */
static uint8_t is_alpha(uint8_t keycode);

/* USB Keycode Character Map */
uint8_t KEYCODES[NUM_KEYS] = {
     0,    0,   '1',  '2',  '3',  '4',  '5',  '6',
    '7',  '8',  '9',  '0',  '-',  '=',  '\b', '\t',     // 0x0F
    'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',
    'o',  'p',  '[',  ']',  '\n',  0,   'a',  's',      // 0x1F
    'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',
    '\'', '`',   0,   '\\', 'z',  'x',  'c',  'v',      // 0x2F
    'b',  'n',  'm',  ',',  '.',  '/',   0,   '*',
    0,              /* 0x38 Alt                         */
    ' ',            /* 0x39 Space                       */
    0,              /* 0x3A CAPS LOCK                   */
    0, 0, 0, 0, 0,  /* 0x39 F1-F5                       */
    0, 0, 0, 0, 0,  /* 0x44 F6-F10                      */
    0,              /* 0x45 Num Lock                    */
    0,              /* 0x46 Scroll Lock                 */
    0,              /* 0x47 Home        /   Keypad 7    */
    0,              /* 0x48 Up Arrow    /   Keypad 8    */
    0,              /* 0x49 Page Up     /   Keypad 9    */
    '-',            /* 0x4A Minus - (Keypad)            */
    0,              /* 0x4B Left Arrow  /   Keypad 4    */
    0,              /* 0x4C             /   Keypad 5    */
    0,              /* 0x4D Right Arrow /   Keypad 6    */
    '+',            /* 0x4E Plus - (Keypad)             */
    0,              /* 0x4F End         /   Keypad 1    */
    0,              /* 0x50 Down Arrow  /   Keypad 2    */
    0,              /* 0x51 Page Down   /   Keypad 3    */
    0,              /* 0x52 Insert      /   Keypad 0    */
    0,              /* 0x53 Delete, Keypad Decimal      */
    0, 0, 0,        /* (0x54 - 0x56)                    */
    0,              /* 0x57 F11                         */
    0,              /* 0x58 F12                         */
    0, 0,           /* (0x59 - 0x5A)                    */
    0,              /* 0x5B Windows Button              */
    0,              /* (0x5C)                           */
    0,              /* 0x5D Menu                        */
    0, 0            /* (0x5E - 0x5F)                    */
};

/* USB Keycode Character Map, with shift pressed */
uint8_t SHIFT_KEYCODES[NUM_KEYS] = {
     0,    0,   '!',  '@',  '#',  '$',  '%',  '^',
    '&',  '*',  '(',  ')',  '_',  '+', '\b', '\t',      // 0x0F
    'Q',  'W',  'E',  'R',  'T',  'Y',  'U',  'I',
    'O',  'P',  '{',  '}',  '\n',  0 ,  'A',  'S',      // 0x1F
    'D',  'F',  'G',  'H',  'J',  'K',  'L',  ':',
    '"',  '~',   0 ,  '|',  'Z',  'X',  'C',  'V',      // 0x2F
    'B',  'N',  'M',  '<',  '>',  '?',   0 ,  '*',
    0,              /* 0x38 Alt                         */
    ' ',            /* 0x39 Space                       */
    0,              /* 0x3A CAPS LOCK                   */
    0, 0, 0, 0, 0,  /* 0x39 F1-F5                       */
    0, 0, 0, 0, 0,  /* 0x44 F6-F10                      */
    0,              /* 0x45 Num Lock                    */
    0,              /* 0x46 Scroll Lock                 */
    0,              /* 0x47 Home        /   Keypad 7    */
    0,              /* 0x48 Up Arrow    /   Keypad 8    */
    0,              /* 0x49 Page Up     /   Keypad 9    */
    '-',            /* 0x4A Minus - (Keypad)            */
    0,              /* 0x4B Left Arrow  /   Keypad 4    */
    0,              /* 0x4C             /   Keypad 5    */
    0,              /* 0x4D Right Arrow /   Keypad 6    */
    '+',            /* 0x4E Plus - (Keypad)             */
    0,              /* 0x4F End         /   Keypad 1    */
    0,              /* 0x50 Down Arrow  /   Keypad 2    */
    0,              /* 0x51 Page Down   /   Keypad 3    */
    0,              /* 0x52 Insert      /   Keypad 0    */
    0,              /* 0x53 Delete, Keypad Decimal      */
    0, 0, 0,        /* (0x54 - 0x56)                    */
    0,              /* 0x57 F11                         */
    0,              /* 0x58 F12                         */
    0, 0,           /* (0x59 - 0x5A)                    */
    0,              /* 0x5B Windows Button              */
    0,              /* (0x5C)                           */
    0,              /* 0x5D Menu                        */
    0, 0            /* (0x5E - 0x5F)                    */
};

// Modifier 'bools'
uint8_t shift_count,    // number of shifts pressed
        shift_flag,     // true if either shift is pressed
        ctrl_flag,      // true if Control is pressed
        alt_flag,       // true is Alt is pressed
        caps_flag,      // true if Caps Lock is active (toggle)
        scroll_flag,    // true if Scroll Lock is active (toggle)
        num_flag,       // true if Num Lock is active (toggle)
        insert_flag;    // true if Insert is active (toggle)


/*
 * Keyboard Init Function
 *
 * Description: Initialize the Keyboard
 * Inputs: None
 * Outputs: None
 * Side Effects: Unmasks IRQ-1 on the PIC
 */
void keyboard_init(){
    // Enable PS/2 Port just in case
    outb(ENABLE1_CMD, PS2_CMD_REG);

    // initialize modifier keys
    shift_count = 0;
    shift_flag = 0;
    ctrl_flag = 0;
    alt_flag = 0;
    caps_flag = 0;
    scroll_flag = 0;
    num_flag = 0;
    insert_flag = 0;

    // clear out the buffer
    clear_buffer(KEY_BUFFER, KEY_BUF_SIZE);

    // enable VGA cursor
    enable_cursor(CURSOR_START, CURSOR_END);

    // Keyboard initialized, ready for interrupts
    enable_irq(KEYBOARD_IRQ);
}

/*
 * Keyboard Interrupt Handler Function
 *
 * Description: Prints character of key pressed on keyboard
 * Inputs: None
 * Outputs: None
 * Side Effects: Prints characters to the Screen
 */
void keyboard_interrupt(){
    uint8_t key_pressed, character;         // local variables
    uint8_t i;
    uint8_t* active_key_array;

    key_pressed = inb(PS2_DATA_PORT);       // read keypress
    send_eoi(KEYBOARD_IRQ);                 // send end of interrupt signal to PIC

    // If buffer is full, ignore input unless user pressed
    // enter or backspace.  Send EOI to wait for next keypress.
    if ((BUF_IDX == (MAX_INDEX)) &&
        (key_pressed != ENTER)  &&
        (key_pressed != BACKSPACE) ) {
        send_eoi(KEYBOARD_IRQ);                 // send end of interrupt signal to PIC
        return;
    }

    switch(key_pressed) {
        ////////////////////////////////////////
        // Modifier Keys                      //
        ////////////////////////////////////////
        case LEFT_SHIFT_PRESS:
        case RIGHT_SHIFT_PRESS:
            shift_count++;
            shift_flag = (shift_count > 0);
            break;

        case LEFT_SHIFT_RELEASE:
        case RIGHT_SHIFT_RELEASE:
            shift_count--;
            shift_flag = (shift_count > 0);
            break;

        case CONTROL_PRESS:
            ctrl_flag = 1;
            break;

        case CONTROL_RELEASE:
            ctrl_flag = 0;
            break;

        case ALT_PRESS:
            alt_flag = 1;
            break;

        case ALT_RELEASE:
            alt_flag = 0;
            break;

        case CAPS_LOCK_PRESS:
            caps_flag ^= 1;     // toggle on/off
            break;

        case SCROLL_LOCK_PRESS:
            scroll_flag ^= 1;   // toggle on/off
            break;

        case NUM_LOCK_PRESS:
            num_flag ^= 1;      // toggle on/off
            break;

        ////////////////////////////////////////
        // Other Keys (navigation, etc.)      //
        ////////////////////////////////////////
        case BACKSPACE:
            if (BUF_IDX > 0) {
                KEY_BUFFER[--BUF_IDX] = '\0';     // clear previous character
                putc(ASCII_DEL);
            }
            break;

        case TAB:
            // if the tab would overflow the buffer, don't print it
            if (BUF_IDX + TAB_LENGTH > MAX_INDEX) {
                break;
            }

            for (i = 0; i < TAB_LENGTH; i++) {
                KEY_BUFFER[BUF_IDX++] = TAB_STRING[i];
            }
            puts(TAB_STRING);   // output 4 spaces
            break;

        case ENTER:
            character = KEYCODES[key_pressed];                  // lookup character
            KEY_BUFFER[BUF_IDX++] = character;                  // put in buffer to copy
            strncpy(TERM_KEY_BUF, KEY_BUFFER, KEY_BUF_SIZE);
            putc(character);
            clear_buffer(KEY_BUFFER, KEY_BUF_SIZE);             // clear keyboard buffer
            BUF_IDX = 0;                                        // reset index to 0
            break;

        // case INSERT:
        //     break;
        //
        // case DELETE:
        //     break;

        ////////////////////////////////////////
        // Alphanumeric Keys                  //
        ////////////////////////////////////////
        default:
            // handle key presses, but not releases
            if (key_pressed < NUM_KEYS) {
                /* If CRTL is pressed, handle control sequences,
                 * such as CTRL+L to clear the screen and CTRL+C
                 * to kill the active program. */
                if (ctrl_flag) {
                    if (key_pressed == L_KEY) {
                        clear();
                    } else if(key_pressed == C_KEY){
                        schedule();
                        // printf("Schedule resolved");
                    }
                    break;
                }

                /* Record the proper character based on the state
                 * of SHIFT and CAPS_LOCK. Note: CAPS_LOCK affects
                 * only letters, and SHIFT will reverse the effect
                 * of CAPS_LOCK. */
                if (is_alpha(key_pressed)) {
                    active_key_array = (caps_flag ^ shift_flag) ? SHIFT_KEYCODES : KEYCODES;
                }
                else {
                    active_key_array = (shift_flag) ? SHIFT_KEYCODES : KEYCODES;
                }

                character = active_key_array[key_pressed];

                /* Gross nested ternary code, saved for posterity */
                // active_key_array = (is_alpha(key_pressed)) ? ((caps_flag ^ shift_flag) ? SHIFT_KEYCODES : KEYCODES) : ((shift_flag) ? SHIFT_KEYCODES : KEYCODES);
                // character = active_key_array[key_pressed];

                KEY_BUFFER[BUF_IDX++] = character;
                cputc(CUSTOM_COLORS[1], character); // output character with keybinding and keyboard text color
            }
            break;
    }   // end switch case

}

/*
 * Buffer Clear Function
 *
 * Description: Sets a certain length of buffer to 0
 * Inputs:
 *      buf - pointer to char buffer
 *      size - number of characters to clear
 * Outputs: None
 * Side Effects: Changes contents of the buffer
 */
void clear_buffer(int8_t* buf, uint32_t size) {
    memset(buf, 0, size);   // set the bytes to '\0'
}

///////////////////////////////////////////////////////////////////////////////

/* Keycodes for character type boundaries */
#define KEY_1 0x02
#define KEY_0 0x0A
#define KEY_Q 0x10
#define KEY_P 0x19
#define KEY_A 0x1E
#define KEY_L 0x26
#define KEY_Z 0x2c
#define KEY_M 0x32

/* Determine if keycode represents an alphabetic character.
 * Returns 1 if true, 0 if false.
 */
uint8_t is_alpha(uint8_t keycode) {
    return ((keycode >= KEY_Q && keycode <= KEY_P) ||   // upper row
            (keycode >= KEY_A && keycode <= KEY_L) ||   // middle row
            (keycode >= KEY_Z && keycode <= KEY_M));    // lower row
}
