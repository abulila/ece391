/* cursor.h - Definitions for cursor
 * tabsize:4, soft tabs
 */

#ifndef _CURSOR_H
#define _CURSOR_H

#include "lib.h"

/* Defines */
/* From Osdever.net VGA Page: http://www.osdever.net/FreeVGA/vga/crtcreg.htm */
#define CURSOR_PORT 0x3D4       // Address Port
#define CURSOR_DATA 0x3D5       // Data Port
#define START_REG 0x0A          // Cursor Start Register
#define END_REG 0x0B            // Cursor End Register
#define LOC_HI_REG 0x0E         // Cursor Location High Register
#define LOC_LO_REG 0x0F         // Cursor Location Low Register
#define LOW_BYTE_MASK 0xFF      // 0xFF mask
#define CURSOR_DIS 0x20         // Set Bit 5 to 1 for disabling cursor
#define CURSOR_START_EN 0xC0    // Set Bit 5 to 0 to enable cursor from start scanline
#define CURSOR_END_DIS 0xE0     // Set Bit 5 to 1 to disable cursor past end scanline

#define SCREEN_WIDTH 80         // Defined Screenwidth from lib.c


/* Functions */
/* Enable the cursor */
void enable_cursor(uint8_t cursor_start, uint8_t cursor_end);
/* Disable the cursor (if required) */
void disable_cursor();
/* Move cursor around the screen */
void update_cursor(int x, int y);

#endif
