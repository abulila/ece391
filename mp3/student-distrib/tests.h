#ifndef TESTS_H
#define TESTS_H

// test launcher
void launch_tests();

/* The size of the video memory (and any other) 4k page in units of 4 byte segments. */
#define VID_MEM_4KB_BY_4B_MAX 1024
/* An address just before video memory (at 0xB8000) */
#define PAGING_INVALID_FIRST4MB_ADDR 0xB7000
/* An address large enough to be beyond the first 8MB */
#define PAGING_INVALID_OVER_8MB_ADDR 0x1000000
/* The address of the video memory */
#define VID_MEM_ADDR 0xB8000
/* The starting adress of kernel memory (at 4mb) */
#define KERNEL_ACCESS_ADDR 0x400000
/* An arbitrary nonzero value to divide by 0 for testing. */
#define NONZERO_TEST_VALUE 5

#endif /* TESTS_H */
