#ifndef _RTC_H
#define _RTC_H

#define RTC_PORT    0x70    /* Port used to specify RTC register number */
#define RTC_CMOS    0x71    /* Port used to read RTC CMOS memory        */

#define RTC_IRQ     8       /* RTC is connected to IRQ 8 */

/* Offsets for RTC control registers A, B, C */
#define RTC_CRA     0x8A
#define RTC_CRB     0x8B
#define RTC_CRC     0x8C

/* Initialization Constants */
#define SET_DV      0x20    // used to set bit 5 of crA (DV = 010)
#define SET_PIE     0x40    // used set bit 6 in crB (PIE = 1)

#define MASK_7      0x80    // used to clear bits 6:0
#define MASK_LOW    0x0F    // used to preserve bits 3:0
#define MASK_HIGH   0xF0    // used to preserve bits 7:4

/* Frequency Constants */
// Bit masks for Control Register A,
// source: ds12887 datasheet, page 19
#define freq0       0x00
#define freq2       0x0F
#define freq4       0x0E
#define freq8       0x0D
#define freq16      0x0C
#define freq32      0x0B
#define freq64      0x0A
#define freq128     0x09
#define freq256     0x08
#define freq512     0x07
#define freq1024    0x06
// rates 0x01 and 0x02 are not usable
// rates 0x03, 0x04, an 0x05 are too fast for our kernel


/* Initialize the RTC */
void rtc_init(void);
/* RTC Interrupt Handler */
void rtc_interrupt(void);

/* System Call Handlers */
int32_t rtc_open(const uint8_t* filename);
int32_t rtc_read(int32_t fd, void* buf, int32_t nbytes);
int32_t rtc_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t rtc_close(int32_t fd);

#endif // _RTC_H
