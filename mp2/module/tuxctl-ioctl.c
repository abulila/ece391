/*
 * tuxctl-ioctl.c
 *
 * Driver (skeleton) for the mp2 tuxcontrollers for ECE391 at UIUC.
 *
 * Mark Murphy 2006
 * Andrew Ofisher 2007
 * Steve Lumetta 12-13 Sep 2009
 * Puskar Naha 2013
 */

#include <asm/current.h>
#include <asm/uaccess.h>

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/file.h>
#include <linux/miscdevice.h>
#include <linux/kdev_t.h>
#include <linux/tty.h>
#include <linux/spinlock.h>

#include "tuxctl-ld.h"
#include "tuxctl-ioctl.h"
#include "mtcp.h"

#define debug(str, ...) printk(KERN_DEBUG "%s: " str, __FUNCTION__, ## __VA_ARGS__)

#define DEBUG 0     // Enable debug print statements. Set to 0 to disable

static int tux_init(struct tty_struct* tty, unsigned long arg);
static int tux_buttons(struct tty_struct* tty, unsigned long arg);
static int tux_set_led(struct tty_struct* tty, unsigned long arg);


/************************ Custom Global Variables *************************/
#define DECIMAL 0x10
unsigned int buttons;               // current state of buttons, active high
unsigned int leds;                  // current state of LEDs, active high
uint8_t wait_for_ack = 0;           // flag indicating we are expecting an ACK response
uint8_t button_state_changed = 0;   // flag indicating buttons have changed
uint8_t reset_buffer[3] = {         // buffer used when initializing TUX
    MTCP_DBG_OFF,
    MTCP_BIOC_ON,
    MTCP_LED_USR
};
uint8_t led_buffer[6];              // buffer used to resend LED state
uint8_t led_buffer_size;            // length of LED buffer (may not be full)
static spinlock_t tux_lock = SPIN_LOCK_UNLOCKED;

static char seven_seg[16] = {
    0xE7 /* 0 - 11100111 */, 0x06 /* 1 - 00000110 */,
    0xCB /* 2 - 11001011 */, 0x8F /* 3 - 10001111 */,
    0x2E /* 4 - 00101110 */, 0xAD /* 5 - 10101101 */,
    0xED /* 6 - 11101101 */, 0x86 /* 7 - 10000110 */,
    0xEF /* 8 - 11101111 */, 0xAF /* 9 - 10101111 */,
    0xEE /* A - 11101110 */, 0x6D /* b - 01101101 */,
    0xE1 /* C - 11100001 */, 0x4F /* d - 01001111 */,
    0xE9 /* E - 11101001 */, 0xE8 /* F - 11101000 */
};

/************************ Protocol Implementation *************************/

/* tuxctl_handle_packet()
 * IMPORTANT : Read the header for tuxctl_ldisc_data_callback() in
 * tuxctl-ld.c. It calls this function, so all warnings there apply
 * here as well.
 */
void tuxctl_handle_packet(struct tty_struct* tty, unsigned char* packet) {
    unsigned a, b, c;
    unsigned long flags;

    a = packet[0]; /* Avoid printk() sign extending the 8-bit */
    b = packet[1]; /* values when printing them. */
    c = packet[2];

    /*printk("packet : %x %x %x\n", a, b, c); */

    switch (a) {
        case MTCP_ACK:
            #if (DEBUG == 1)
                printk("ACK received\n");
            #endif
            // Clear the ACK flag, under the protection of the tux_lock
            spin_lock_irqsave(&tux_lock, flags);
            wait_for_ack = 0;
            spin_unlock_irqrestore(&tux_lock, flags);
            break;
        case MTCP_BIOC_EVENT:
            #if (DEBUG == 1)
                printk("TUX Button Event\n");
            #endif

            /*                __7___6___5_____4_____3___2___1___0__
             * Input format:  | C | B | A | START | R | D | L | U |
             *   active low   +---+---+---+-------+---+---+---+---+
             *
             *                __7___6___5___4___3___2___1_____0____
             * Output format: | R | L | D | U | C | B | A | START |
             *   active high  +---+---+---+---+---+---+---+-------+
            */
            b = ~b;
            c = ~c;

            spin_lock_irqsave(&tux_lock, flags);
            // save C B A START to lower nibble
            // save R L D U to upper nibble (note the middle bits are swapped)
            buttons = (b & 0x0F);
            buttons |= ((c & 0x09) | ((c & 0x02) << 1) | ((c & 0x04) >> 1)) << 4;
            button_state_changed = 1;
            spin_unlock_irqrestore(&tux_lock, flags);
            break;
        case MTCP_RESET:
            #if (DEBUG == 1)
                printk("Tux Reset\n");
            #endif

            /* Tux Controller was reset... perform psuedo-initialization
             * by re-enabling BIOC and LED_USR mode, then set the LEDs to
             * their previous state.
             */
            spin_lock_irqsave(&tux_lock, flags);
            wait_for_ack = 1;   // wait for acknowlegement of configuration
            tuxctl_ldisc_put(tty, reset_buffer, sizeof(reset_buffer));  // reinitialize
            tuxctl_ldisc_put(tty, led_buffer, led_buffer_size);         // restore display
            spin_unlock_irqrestore(&tux_lock, flags);

            break;
        case MTCP_ERROR:
            #if (DEBUG == 1)
                printk("ERROR: unrecognized command\n");
            #endif
            break;
        default:
            #if (DEBUG == 1)
                printk("Unhandled response: %x %x %x\n", a, b, c);
            #endif
            break;
    }
}

/******** IMPORTANT NOTE: READ THIS BEFORE IMPLEMENTING THE IOCTLS ************
 *                                                                            *
 * The ioctls should not spend any time waiting for responses to the commands *
 * they send to the controller. The data is sent over the serial line at      *
 * 9600 BAUD. At this rate, a byte takes approximately 1 millisecond to       *
 * transmit; this means that there will be about 9 milliseconds between       *
 * the time you request that the low-level serial driver send the             *
 * 6-byte SET_LEDS packet and the time the 3-byte ACK packet finishes         *
 * arriving. This is far too long a time for a system call to take. The       *
 * ioctls should return immediately with success if their parameters are      *
 * valid.                                                                     *
 *                                                                            *
 ******************************************************************************/

int tuxctl_ioctl(struct tty_struct* tty, struct file* file,
                 unsigned cmd, unsigned long arg) {
    #if (DEBUG == 1)
        printk("tuxctl_ioctl: ");
    #endif

    switch (cmd) {
        case TUX_INIT:
            return tux_init(tty, arg);
        case TUX_BUTTONS:
            return tux_buttons(tty, arg);
        case TUX_SET_LED:
            return tux_set_led(tty, arg);
        default:
            return -EINVAL;
    }
}

/*
 * tux_init
 *   DESCRIPTION: initialize TUX controller to send button interrupts and set LEDs
 *   INPUTS: tty - TUX controller serial connection
 *           arg - ignored
 *   OUTPUTS: none
 *   RETURN VALUE: 0
 *   SIDE EFFECTS: initializes TUX controller
 */
static int tux_init(struct tty_struct* tty, unsigned long arg) {
    unsigned long flags;

    #if (DEBUG == 1)
        printk("TUX_INIT\n");
    #endif

    (void)arg;  // ignore

    spin_lock_irqsave(&tux_lock, flags);
    wait_for_ack = 0;   // reset ack wait flag
    tuxctl_ldisc_put(tty, reset_buffer, sizeof(reset_buffer));
    spin_unlock_irqrestore(&tux_lock, flags);

    return 0;
}

/*
 * tux_buttons
 *   DESCRIPTION: update user space button variable with current state of buttons
 *   INPUTS: tty - TUX controller serial connection
 *           arg - pointer to user space variable
 *   OUTPUTS: none
 *   RETURN VALUE: 0, or -EINVAL if pointer is not valid
 *   SIDE EFFECTS: sets value of user space variable
 */
static int tux_buttons(struct tty_struct* tty, unsigned long arg) {
    unsigned long* ptr = (unsigned long*)arg;
    unsigned long flags;

    #if (DEBUG == 1)
        printk("TUX_BUTTONS\n");
    #endif

    (void)tty;

    if (ptr == NULL) {
        return -EINVAL;
    }

    spin_lock_irqsave(&tux_lock, flags);
    // only write to user space if buttons have changed
    if (button_state_changed) {
        button_state_changed = 0;
        copy_to_user(ptr, &buttons, sizeof(buttons));
    }
    spin_unlock_irqrestore(&tux_lock, flags);

    return 0;
}

/*
 * tux_set_led
 *   DESCRIPTION: set the LEDs based on packed data
 *   INPUTS: tty - TUX controller serial connection
 *           arg - packed data for LED display
 *   OUTPUTS: none
 *   RETURN VALUE: 0
 *   SIDE EFFECTS: sets the TUX controller display
 */
static int tux_set_led(struct tty_struct* tty, unsigned long arg) {
    unsigned long flags;
    unsigned short value;         // 16 bit value to display
    unsigned char led_mask;       // bitmask indicating which digits are active
    unsigned char dec_mask;       // bitmask indicating which decimal points are active
    unsigned char led_value[4];   // value separated by digit
    unsigned char led_state[4];   // led active state by digit
    unsigned char dec_state[4];   // decimal active state by digit
    unsigned char packet[6] = {0};

    int i;      // looping variable
    int index;  // current index in packet

    #if (DEBUG == 1)
        printk("TUX_SET_LED: %lx\n", arg);
    #endif

    // unpack the data from arg
    value    = (arg & 0x0000FFFF);
    led_mask = (arg & 0x00FF0000) >> 16;
    dec_mask = (arg & 0xFF000000) >> 24;

    // extract individual digit data for easier looping
    for (i = 0; i < 4; i++) {
        led_value[i] = ((value & (0x0F << (4*i))) >> (4*i));
        led_state[i] = (led_mask & (1 << i)) ? 1 : 0;
        dec_state[i] = (dec_mask & (1 << i)) ? 1 : 0;
    }

    packet[0] = MTCP_LED_SET;
    index = 2;

    for (i = 0; i < 4; i++) {
        if (led_state[i] || dec_state[i]) {
            packet[1] |= (1 << i);  // set the appropriate LED ON bit in byte 1
            if (led_state[i]) {
                packet[index] |= seven_seg[led_value[i]];
            }
            if (dec_state[i]) {
                packet[index] |= DECIMAL;   // set the decimal bit
            }
            index++;
        }
    }

    spin_lock_irqsave(&tux_lock, flags);
    leds = arg;     // save new LED state for later
    for (i = 0; i < index; i++) {
        led_buffer[i] = packet[i];
    }
    led_buffer_size = index;

    // if waiting for an ACK, don't set the LEDs
    if (wait_for_ack) {
        spin_unlock_irqrestore(&tux_lock, flags);
        return 0;
    }

    // set wait_for_ack to prevent spamming
    wait_for_ack = 1;
    tuxctl_ldisc_put(tty, led_buffer, led_buffer_size);
    spin_unlock_irqrestore(&tux_lock, flags);

    return 0;
}
